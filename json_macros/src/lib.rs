//! Macros to generate JSON parsing for data structures

extern crate proc_macro as pm;
extern crate proc_macro2 as pm2;

#[macro_use]
mod result;
mod path;
mod cmd;

macro_rules! cond {
	{
		$($test:expr => $arm:expr,)+
		else => $else:expr $(,)?
	} => {
		$(if $test { $arm } else )+
		{ $else }
	}
}

pub(crate) use result::*;
use path::*;
use cmd::gen_json_path;

use quote::{quote, TokenStreamExt, ToTokens, format_ident};
use syn::{parse_macro_input, DeriveInput, Data, spanned::Spanned, Error};
use pm2::{Span, TokenStream, TokenTree};

trait IteratorExt: Iterator + Sized {
	fn try_only(mut self) -> SResult<Option<Self::Item>> {
		let result = match self.next() {
			None => return Ok(None),
			Some(result) => result,
		};
		match self.next() {
			None => Ok(Some(result)),
			Some(_) => Err(StaticError("Expected only one item of this kind")),
		}
	}

	fn only(mut self) -> SResult<Self::Item> {
		let result = match self.next() {
			Some(result) => result,
			None => return Err(StaticError("Expected exactly one item of this kind")),
		};
		match self.next() {
			None => Ok(result),
			Some(_) => Err(StaticError("Expected exactly one item of this kind")),
		}
	}
}

trait SpannedExt {
	fn span_fix(&self) -> pm2::Span;
}

impl SpannedExt for syn::Attribute {
	fn span_fix(&self) -> pm2::Span {
		self.bracket_token.span
	}
}

impl<T: Iterator> IteratorExt for T {}

fn json_item_impl(item: DeriveInput) -> pm2::TokenStream {
	let ident = &item.ident;
	let mut body = TokenStream::new();
	// TODO: Make this more efficient
	let item_span = item.span();
	match item.data {
		Data::Struct(data) => {
			enum Mode {
				Dict,
				Tuple,
			}
			let mode = match data.fields {
				syn::Fields::Named(_) => Mode::Dict,
				syn::Fields::Unnamed(_) => Mode::Tuple,
				syn::Fields::Unit => report_error!(data.fields.span() => "Unit structs aren't valid JSON types"),
			};
			for (idx, field) in data.fields.iter().enumerate() {
				enum AttrKind {
					Key,
					Default,
					Custom,
				}
				let (attr, kind) = se_unwrap!(field.span() => field.attrs.iter().filter_map(|a|
				cond! {
					a.path.is_ident("json_key") => Some((a, AttrKind::Key)),
					a.path.is_ident("json_default") => Some((a, AttrKind::Default)),
					a.path.is_ident("json_custom") => Some((a, AttrKind::Custom)),
					else => None,
				}).only());

				#[derive(Clone, Copy)]
				enum FieldId<'i> {
					Dict(&'i syn::Ident),
					Tuple(usize),
				}
				impl From<FieldId<'_>> for Path {
					fn from(id: FieldId<'_>) -> Self {
						match id {
							FieldId::Dict(key) => Path::new_key(key.to_string()),
							FieldId::Tuple(idx) => Path::new_idx(idx),
						}
					}
				}
				impl quote::IdentFragment for FieldId<'_> {
					fn fmt(&self, f: &mut core::fmt::Formatter) -> core::fmt::Result {
						match *self {
							FieldId::Dict(key) => key.fmt(f),
							FieldId::Tuple(idx) => idx.fmt(f),
						}
					}

					fn span(&self) -> Option<Span> {
						match *self {
							FieldId::Dict(key) => Some(key.span()),
							FieldId::Tuple(_) => None,
						}
					}
				}
				impl quote::ToTokens for FieldId<'_> {
					fn to_tokens(&self, tokens: &mut TokenStream) {
						match *self {
							FieldId::Dict(key) => tokens.extend([
								TokenTree::Ident(key.clone()),
							]),
							FieldId::Tuple(idx) => tokens.extend([
								TokenTree::Literal(pm2::Literal::usize_unsuffixed(idx)),
							]),
						}
					}
				}

				// Guarded by above check
				let id = match mode {
					Mode::Dict => FieldId::Dict(field.ident.as_ref().unwrap()),
					Mode::Tuple => FieldId::Tuple(idx),
				};
				let mut handler = TokenStream::new();
				match kind {
					AttrKind::Key => {
						let key = if attr.tokens.is_empty() {
							Ok(JsonKey::new())
						} else {
							attr.parse_args()
						};
						let mut key = tt_unwrap!(key);
						if key.path.is_none() {
							key.path = Some(Path::from(id))
						}
						handler.append_all([
							pm2::Ident::new("tree", Span::call_site()),
							//pm2::Punct::new(',', pm2::Spacing::Alone),
						]);
						let commands = tt_unwrap!(gen_json_path(&key, &field.ty));
						commands.join_token_stream(&mut handler);
					}
					AttrKind::Default => {
						let nv = match attr.parse_meta() {
							Ok(syn::Meta::NameValue(nv)) => nv,
							Err(err) => return err.into_compile_error().into(),
							_ => report_error!(attr.span_fix() => "default attribute must have an assigned value"),
						};
						nv.lit.to_tokens(&mut handler);
					}
					AttrKind::Custom => {
						/*let custom_name = tt_unwrap!(syn::parse::Parser::parse2(|input: syn::parse::ParseStream| {
							Ok(if input.is_empty() {
								None
							} else {
								let _eq: syn::token::Eq = input.parse()?;
								let ident: syn::Ident = input.parse()?;
								Some(ident)
							})
						}, attr.tokens.clone()));*/
						let custom_name = if attr.tokens.is_empty() {
							format_ident!("_default_{}", id)
						} else {
							tt_unwrap!(attr.parse_args())
						};
						handler.extend([
							TokenTree::Ident(pm2::Ident::new("Self", Span::call_site())),
							TokenTree::Punct(pm2::Punct::new(':', pm2::Spacing::Joint)),
							TokenTree::Punct(pm2::Punct::new(':', pm2::Spacing::Alone)),
							TokenTree::Ident(custom_name),
							TokenTree::Group(pm2::Group::new(pm2::Delimiter::Parenthesis, TokenStream::new())),
							// When the function returns a result
							TokenTree::Punct(pm2::Punct::new('?', pm2::Spacing::Alone)),
						]);
					}
				}
				body.extend(quote!{
					#id: #handler,
				});
			}
			body = quote! { Ok(Self { #body }) };
		},
		Data::Enum(data) => {
			enum Mode {
				None, String, Number,
			}
			struct SpellingAlternatives(Vec<syn::LitStr>);
			enum SpellingKind {
				Alternatives(SpellingAlternatives),
				Excluded,
			}
			impl SpellingAlternatives {
				fn has_some(&self) -> bool {
					!self.0.is_empty()
				}
				fn has_none(&self) -> bool {
					self.0.is_empty()
				}
				fn push(&mut self, alternative: syn::LitStr) {
					self.0.push(alternative);
				}
			}
			impl ToTokens for SpellingAlternatives {
				fn to_tokens(&self, tokens: &mut TokenStream) {
					tokens.append_all(&self.0);
				}
			}
			let mut mode = Mode::None;
			let mut inner = TokenStream::new();
			for var in data.variants {
				let name = &var.ident;
				let spelling = var.attrs
					.iter()
					.filter(|a| a.path.is_ident("json_spelling"))
					.map(|attr|
						match attr.parse_meta()? {
							syn::Meta::NameValue(syn::MetaNameValue{ lit: syn::Lit::Str(repr), .. }) => Ok(Some(repr)),
							syn::Meta::List(list) if list.nested.len() == 1 &&
								matches!(list.nested.first().unwrap(), syn::NestedMeta::Meta(syn::Meta::Path(path)) if path.is_ident("exclude"))
							=> Ok(None),
							_ => return_error!(attr.span() => "Spelling attribute must have a literal value"),
						})
					.fold(Ok(SpellingKind::Alternatives(SpellingAlternatives(Vec::new()))),
						|mut state, spelling| {
							match (&mut state, spelling) {
								(Ok(SpellingKind::Alternatives(ref mut spellings)), Ok(Some(spell))) => {
									spellings.push(spell);
									state
								}
								(Ok(SpellingKind::Alternatives(ref alts)), Ok(None)) if alts.has_none() => {
									Ok(SpellingKind::Excluded)
								}
								(Ok(SpellingKind::Excluded), Ok(None)) => state,
								(Ok(SpellingKind::Alternatives(ref alts)), Ok(None)) => {
									return_error!(alts.span() => "Cannot exclude variants with assigned spellings")
								}
								(Ok(SpellingKind::Excluded), Ok(Some(spell))) => {
									return_error!(spell.span() => "Cannot assign spellings to excluded variants")
								}
								(Err(_), Ok(_)) => state,
								(Ok(_), Err(error)) => Err(error),
								(Err(ref mut errors), Err(error)) => {
									errors.combine(error);
									state
								}
							}
					});

				let spelling = match tt_unwrap!(spelling) {
					SpellingKind::Alternatives(alts) => alts,
					SpellingKind::Excluded => continue,
				};

				if !matches!(var.fields, syn::Fields::Unit) {
					report_error!(var.span() => "Only unit variants are supported");
				}
				if let Some((_, ref val)) = var.discriminant {
					if matches!(mode, Mode::String) {
						report_error!(val.span() => "Cannot mix string and integer indices");
					}
					if spelling.has_some() {
						report_error!(spelling.span() => "Spelling can only be specified for string enums");
					}
					mode = Mode::Number;
					inner.extend(quote! { #val => Ok(#ident::#name), });
				} else if matches!(mode, Mode::Number) {
					report_error!(name.span() => "Cannot mix string and integer indices");
				} else {
					mode = Mode::String;
					if spelling.has_none() {
						syn::LitStr::new(name.to_string().as_str(), name.span()).to_tokens(&mut inner);
					} else {
						inner.append_separated(spelling.0, pm2::Punct::new('|', pm2::Spacing::Alone));
					};
					inner.extend(quote! { => Ok(#ident::#name), });
				}
			}
			body = match mode {
				Mode::None => report_error!(item_span => "Enum has no variants"),
				Mode::String => quote! {
					match tree.as_str().ok_or(json::ParseError)? {
						#inner
						_ => Err(json::ParseError),
					}
				},
				Mode::Number => quote! {
					match tree.as_isize().ok_or(json::ParseError)? {
						#inner
						_ => Err(json::ParseError),
					}
				},
			};
		},
		Data::Union(_) => report_error!(item_span => "Union parsing is not supported"),
	}
	quote! {
		impl json::JsonParse for #ident {
			fn parse(tree: &json::JsonValue) -> json::Result<Self> {
				#body
			}
		}
	}
}

/// Example
/// ```ignore
/// #[derive(JsonParse)]
/// struct MyData {
/// 	#[json_key]
/// 	foo: String,
/// 	#[json_key("egg.xyz")]
/// 	bar: Vec<MyItem>,
/// }
///
/// #[derive(JsonParse)]
/// struct MyItem {
/// 	#[json_key]
/// 	name: String,
/// 	#[json_key]
/// 	value: JsonValue,
/// 	#[json_key(allow_missing)]
/// 	attr: Option<i32>,
/// }
/// ```
///
/// Schema
/// ```json
/// {
/// 	"foo": string,
/// 	"egg": {
/// 		"xyz": [
/// 			{
/// 				"name": string,
/// 				"value": value,
/// 				"attr"?: number
/// 			}
/// 		]
/// 	}
/// }
/// ```
///
/// Gen
/// ```ignore
/// impl json::JsonParse for MyData {
/// 	fn parse(tree: &json::JsonValue) -> Option<Self> {
/// 		Some(Self {
/// 			foo: tree.safe_index_obj("foo").and_then(|cont| cont.as_str())?.into(),
/// 			bar: tree.safe_index_obj("egg").and_then(|cont| cont.safe_index_obj("xyz"))?.collect_arr(
/// 				|item| <MyItem as json::JsonParse>::parse(item)
/// 			)?,
/// 		})
/// 	}
/// }
/// impl json::JsonParse for MyItem {
/// 	fn parse(tree: &json::JsonValue) -> Option<Self> {
/// 		Some(Self {
/// 			name: tree.safe_index_obj("name").and_then(|cont| cont.as_str())?.into(),
/// 			value: tree.safe_index_obj("value")?.clone(),
/// 			// TODO: Extract error from "parse" but not from "safe_index_obj"
/// 			attr: tree.safe_index_obj("attr").and_then(|cont| cont.as_i32()),
/// 		})
/// 	}
/// }
/// ```
///
/// If a struct field is not present in the JSON scheme, [json_default] attribute can
/// be used to specify a default value.
///
/// Alternatively, [json_custom] attribute can be used along with a method named _default_{field_name}
/// or one provided as argument, to generate the value.
/// ```ignore
/// struct MyStruct {
/// 	#[json_key("meta.field")]
/// 	json_field: i32,
/// 	#[json_key]
/// 	data: JsonValue,
/// 	#[json_default = false]
/// 	is_used: bool,
/// 	#[json_custom]
/// 	data_type: String,
/// 	#[json_custom = gen_path]
/// 	path: String,
/// }
///
/// impl MyStruct {
/// 	#[inline(always)]
/// 	fn _default_data_type() -> json::Result<String> {
/// 		Ok("any".into())
/// 	}
/// 	#[inline]
/// 	fn gen_path() -> json::Result<String> {
/// 		Ok("/default".into())
/// 	}
/// }
/// ```
///
/// Generated output will be like this
/// ```ignore
/// impl json::JsonParse for MyData {
/// 	fn parse(tree: &json::JsonValue) -> json::Result<Self> {
/// 		Ok(Self {
/// 			json_field: tree.safe_index_obj("meta").and_then(|cont| cont.safe_index_obj("field")).ok_or(json::ParseError)?.as_i32().ok_or(json::ParseError)?,
/// 			data: tree.safe_index_obj("data").ok_or(json::ParseError)?.clone(),
/// 			is_used: false,
/// 			data_type: Self::_default_data_type()?,
/// 			path: Self::gen_path()?,
/// 		})
/// 	}
/// }
/// ```
#[proc_macro_derive(JsonParse, attributes(json_key, json_spelling, json_default, json_custom))]
pub fn json_item(item: pm::TokenStream) -> pm::TokenStream {
	json_item_impl(parse_macro_input!(item as DeriveInput)).into()
}

#[cfg(test)]
mod test {
	use super::json_item_impl;
	use syn::DeriveInput;
	use quote::quote;

	fn json_item(item: pm2::TokenStream) -> pm2::TokenStream {
		json_item_impl(syn::parse2::<DeriveInput>(item).unwrap())
	}

	#[test]
	fn basic() {
		let my_data = quote! {
			struct MyData {
				#[json_key]
				foo: String,
				#[json_key("egg.xyz")]
				bar: Vec<MyItem>,
			}
		};

		let my_item = quote! {
			struct MyItem {
				#[json_key]
				name: String,
				#[json_key]
				value: JsonValue,
				#[json_key(allow_missing)]
				attr: Option<i32>,
			}
		};

		let my_data_gen = json_item(my_data);
		let my_item_gen = json_item(my_item);

		println!("{}", my_data_gen);
		println!("{}", my_item_gen);

		println!("{}", json_item(quote! {
			struct TestOption {
				#[json_key]
				required: String,
				#[json_key(allow_missing)]
				optional: Option<String>,
				#[json_key]
				nullable: Option<String>,
				#[json_key(allow_missing)]
				optional_nullable: Option<Option<String>>,
				#[json_key(allow_missing)]
				optional_int: Option<i32>,
			}
		}));

		// X?.y()? -> X.and_then(|cont| cont.y())?
		// X?.y() \-\> X.and_then(|cont| cont.y())
		// X?.y() -> X.map(|cont| cont.y())?
		// X.and_then(|cont| cont.y()) <- (X?.y()?)^

		// New model:
		// Option(X) -> Capture(X.parse())
		// Nullable(X) -> null_or(X.parse())

		#[cfg(never)]
		impl JsonParse for TestOption {
			fn parse(tree: &json::JsonValue) -> Option<Self> {
				Ok(Self {
					//required: tree.safe_index_obj("").and_then(|cont| cont.as_str())?.into(),
					//optional: tree.safe_index_obj("").and_then(|cont| cont.as_str())./*s/and_then*/map(|cont| cont.into()),
					//nullable: tree.safe_index_obj("").and_then(|cont| cont.null_or(|cont| /*Some(*/ cont.as_str()?.into()))?,
					//optional_nullable: tree.safe_index_obj("").and_then(|cont| cont.null_or(|cont| /*Some(*/ cont.as_str()?.into())),
					//optional_int: tree.safe_index_obj("").and_then(|cont| cont.as_i32()),
					required: tree.safe_index_obj("").ok_or(())?.as_str().ok_or(())?.into(),
					optional: tree.safe_index_obj("").missing_or(|cont| Ok(cont.as_str().ok_or(())?.into()))?,
					nullable: tree.safe_index_obj("").ok_or(())?.null_or(|cont| Ok(cont.as_str().ok_or(())?.into()))?,
					optional_nullable: tree.safe_index_obj("").missing_or(|cont| cont.null_or(|cont| Ok(cont.as_str().ok_or(())?.into())))?,
					optional_int: tree.safe_index_obj("").missing_or(|cont| cont.as_i32().ok_or(()))?,
				})
			}
		}

		#[cfg(never)] {
		impl JsonParse for MyData {
			fn parse(tree: &json::JsonValue) -> json::Result<Self> {
				Ok(Self {
					foo: tree.safe_index_obj("foo").ok_or(())?.as_str().ok_or(())?.into(),
					bar: tree.safe_index_obj("egg").and_then(|cont| cont.safe_index_obj("xyz")).ok_or(())
						?.collect_arr(|item| <MyItem>::parse(item))?,
				})
			}
		}

		impl JsonParse for MyData {
			fn parse(tree: &json::JsonValue) -> Option<Self> {
				Some(Self {
					foo: tree.safe_index_obj("foo").ok_or(())?.as_str()?.into(),
					bar: tree.safe_index_obj("egg").and_then(|cont| cont.safe_index_obj("xyz")).ok_or(())
						?.collect_arr(|item| <MyItem>::parse(item))?,
				})
			}
		}
		impl JsonParse for MyItem {
			fn parse(tree: &json::JsonValue) -> Option<Self> {
				Some(Self {
					name: tree.safe_index_obj("name").ok_or(())?.as_str()?.into(),
					value: tree.safe_index_obj("value").ok_or(())?.clone(),
					attr: tree.safe_index_obj("attr").missing_or(|cont| Ok(cont.as_i32()?))?,
				})
			}
		}

		impl JsonParse for TestOption {
			fn parse(tree: &json::JsonValue) -> Option<Self> {
				Some(Self {
					required: tree.safe_index_obj("").ok_or(())?.as_str()?.into(),
					optional: tree.safe_index_obj("").missing_or(|cont| Ok(cont.as_str()?.into()))?,
					nullable: tree.safe_index_obj("").ok_or(())?.null_or(|cont| Ok(cont.as_str()?.into()))?,
					optional_nullable: tree.safe_index_obj ("").missing_or(|cont| Ok(cont.null_or(|cont| Ok(cont.as_str()?.into()))?))?,
					optional_int: tree.safe_index_obj("").missing_or(|cont| Ok(cont.as_i32()?))?,
				})
			}
		}
		}

		/*assert_eq!(my_data_gen, quote! {
			impl JsonParse for MyData {
				fn parse(tree: &json::JsonValue) -> Option<Self> {
					Some(Self {
						foo: tree.safe_index_obj("foo").and_then(|cont| cont.as_str())?.into(),
						bar: tree.safe_index_obj("egg").and_then(|cont| cont.safe_index_obj("xyz"))?.collect_arr(
							|item| MyItem::parse(item)
						)?,
					})
				}
			}
		});

		assert_eq!(my_item_gen, quote! {
			impl JsonParse for MyItem {
				fn parse(tree: &json::JsonValue) -> Option<Self> {
					Some(Self {
						name: tree.safe_index_obj("name").and_then(|cont| cont.as_str())?.into(),
						value: tree.safe_index_obj("value")?.clone(),
						// TODO: Extract error from "parse" but not from "safe_index_obj"
						attr: tree.safe_index_obj("attr").and_then(|cont| cont.as_i32()),
					})
				}
			}
		});*/
	}
}
