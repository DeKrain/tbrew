use syn::Result;
use super::*;
use crate::path::{JsonKey, ObjectKey, ArrayIndex};

pub fn gen_json_path(key: &JsonKey, typ: &syn::Type) -> Result<ParseCommands> {
	let mut comms = ParseCommands::new();

	/*macro_rules! parse_commands {
		{
			$(
				($($)+ $tk:ident : $tktp:ident ...) => [$($act:tt)+ ...];
			)*
			$(
				($($tp:tt)+) => [$($tpact:tt)+ ...];
			)*
		} => {}
	}*/

	/*macro parse_commands(
		$(
			($($pat:tt)+) => [$($act:tt)+];
		)*
	 ) {}

	parse_commands! {
		($key:key ...) => [ObjIdx($key)? ...];
		($idx:idx ...) => [ArrIdx($key)? ...];
		(String) => [AsStr? Into];
		($t:nullable) => [^$t];
		// TODO: allow `null` as value
		(Option<$t:ty>) => [];
		[$t:int] => [Parse];
		[$t:float] => [Parse];
		[bool] => 
		(Vec<$t:ty>) => [Collect($t)?];
		(JsonValue) => [];
		($t:ty) => [RecParse($t)?];
	}*/

	/*macro_rules! expand_ {
		[] => {}
		[AsStr $($t:tt)*] => {
			comms.then(ParseStep::As("str"));
			expand_!($($t)+);
		}
	}*/

	for path in &key.path.as_ref().expect("Someone forgot to assign the value!").inner {
		match path {
			ObjectKey(key) => {
				comms.then(ParseStep::ObjIdx(key.as_str()));
				comms.propagate();
			}
			ArrayIndex(idx) => {
				comms.then(ParseStep::ArrIdx(*idx));
				comms.propagate();
			}
		}
	}
	let typ = parse_type(&typ, key.allow_missing)?;
	handle_type(&mut comms, typ);
	Ok(comms)
}

fn handle_type(comms: &mut ParseCommands, typ: TypeCategory) {
	macro_rules! expand__ {
		{$dol:tt $(
			[$($pat:tt)+] => {
				$($exp: tt)+
			};
		)*} => {
			macro_rules! expand_ {
				[] => {};
				$(
					[$($pat)+ $dol($dol t:tt)*] => {
						$($exp)+;
						expand_!($dol($dol t)*);
					};
				)*
			}
		};
	}

	expand__! { $
		[AsStr] => { comms.then(ParseStep::As("str")) };
		[AsBool] => { comms.then(ParseStep::As("bool")) };
		[Into] => { comms.then(ParseStep::Into) };
		[Clone] => { comms.then(ParseStep::Clone) };
		[Parse $typ:ident] => { comms.then(ParseStep::As($typ.as_str())) };
		[?] => { comms.propagate() };
		[^] => { comms.unpropagate() };
		[+ $inner:ident] => { handle_type(comms, *$inner) };
	}

	macro_rules! expand {
		[$($tt:tt)*] => {
			{
				expand_!($($tt)*);
			}
		};
	}

	match typ {
		TypeCategory::String => expand![AsStr? Into],
		TypeCategory::JsonValue => expand![Clone],
		TypeCategory::Bool => expand![AsBool?],
		TypeCategory::Option(inner) => expand![+inner ^],
		TypeCategory::Nullable(inner) => {
			let mut steps = ParseCommands::new();
			handle_type(&mut steps, *inner);
			comms.then(ParseStep::NullOr(steps));
			comms.propagate();
		}
		TypeCategory::Int(typ) => expand![Parse typ?],
		TypeCategory::Float(typ) => expand![Parse typ?],
		TypeCategory::Vec(inner) => {
			let mut steps = ParseCommands::new();
			handle_type(&mut steps, *inner);
			comms.then(ParseStep::Collect(steps));
			comms.propagate();
		}
		TypeCategory::HashMap(inner) => todo!(),
		TypeCategory::Parsed(typ) => {
			comms.then(ParseStep::RecParse(typ));
		}
	}
}

#[cfg(test)]
mod test {
	use super::{TypeCategory, ParseCommands, handle_type as handle_type_impl, IntType, FloatType};

	fn handle_type(typ: TypeCategory) -> ParseCommands {
		let mut comms = ParseCommands::new();
		handle_type_impl(&mut comms, typ);
		comms
	}

	#[test]
	fn types() {
		let tp = TypeCategory::Int(IntType::i32);
		let cmd = handle_type(tp);
		dbg!(cmd);

		macro_rules! cases {
			[$(
				$typ:expr $(=> $res:expr)?
			),+ $(,)?] => {
				$(
					dbg!(handle_type($typ));
				)*
			};
		}

		cases! [
			TypeCategory::Option(Box::new(TypeCategory::String)),
			TypeCategory::Nullable(Box::new(TypeCategory::String)),
			TypeCategory::Option(Box::new(TypeCategory::Float(FloatType::f32))),
			TypeCategory::Option(Box::new(
				TypeCategory::Vec(Box::new(
					TypeCategory::Float(FloatType::f32)
				))
			)),
			//TypeCategory::Option(Box::new(TypeCategory::JsonValue)),
		];
	}
}
