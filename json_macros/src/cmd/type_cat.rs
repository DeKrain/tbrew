use syn::{Result, spanned::Spanned};

pub enum TypeCategory {
	String,
	JsonValue,
	Bool,
	// This is used when allow_missing is present
	Option(Box<TypeCategory>),
	// This is used for cases where null is acceptable
	Nullable(Box<TypeCategory>),
	Int(IntType),
	Float(FloatType),
	Vec(Box<TypeCategory>),
	HashMap(Box<TypeCategory>),
	// Used when all other options don't fit
	Parsed(syn::Type),
}

macro_rules! def_num_type {
	( $name:ident $($cases:ident)+ ) => {
		#[allow(non_camel_case_types)]
		pub enum $name {
			$($cases,)+
		}

		impl $name {
			pub fn as_str(self) -> &'static str {
				use self::$name::*;
				match self {
					$($cases => stringify!($cases),)+
				}
			}

			pub fn from_str(str: &str) -> Self {
				use self::$name::*;
				match str {
					$(stringify!($cases) => $cases,)+
					_ => unreachable!(),
				}
			}
		}
	};
}

def_num_type! {
	IntType
	i8 u8 i16 u16 i32 u32 i64 u64 /*unsupported by json parser: i128 u128*/ isize usize
}

def_num_type! {
	FloatType
	f32 f64
}


enum Generics<'a> {
	None,
	Single(&'a syn::Type),
	Double(&'a syn::Type, &'a syn::Type),
}

fn try_unpack_generics(args: &syn::PathArguments) -> Generics {
	use self::Generics::*;

	match args {
		syn::PathArguments::AngleBracketed(args) => match args.args.len() {
			1 => match args.args.first().unwrap() {
				syn::GenericArgument::Type(ty) => Single(ty),
				_ => None,
			},
			2 => match (&args.args[0], &args.args[1]) {
				(syn::GenericArgument::Type(t1), syn::GenericArgument::Type(t2)) => Double(t1, t2),
				_ => None,
			},
			_ => None
		},
		_ => None,
	}
}

pub fn parse_type(typ: &syn::Type, allow_missing: bool) -> Result<TypeCategory> {
	match typ {
		syn::Type::Group(inner) => parse_type(inner.elem.as_ref(), allow_missing),
		syn::Type::Paren(inner) => parse_type(inner.elem.as_ref(), allow_missing),
		syn::Type::Path(path) => {
			if path.qself.is_some() {
				return_error!(path.span() => "Invalid member type")
			}
			let Some(comp) = path.path.segments.last() else {
				return_error!(path.span() => "Invalid member type");
			};
			let ident = &comp.ident;
			if allow_missing {
				return match (ident.to_string().as_str(), &comp.arguments) {
					("Option", syn::PathArguments::AngleBracketed(args))
						if args.args.len() == 1 && matches!(args.args.first().unwrap(), syn::GenericArgument::Type(_))
						=> Ok(TypeCategory::Option(Box::new(parse_type(match args.args.first().unwrap() { syn::GenericArgument::Type(t) => t, _ => unreachable!() }, false)?))),
					_ => return_error!(path.span() => "Expected an Option<> for potentially-missing value"),
				}
			}
			Ok(match ident.to_string().as_str() {
				int_type @ ("i8" | "u8" | "i16" | "u16" | "i32" | "u32" | "i64" | "u64" | "i128" | "u128" | "isize" | "usize")
					=> TypeCategory::Int(IntType::from_str(int_type)),
				float_type @ ("f32" | "f64") => TypeCategory::Float(FloatType::from_str(float_type)),
				"bool" => TypeCategory::Bool,
				"String" => TypeCategory::String,
				"PathBuf" => TypeCategory::String,
				"JsonValue" => TypeCategory::JsonValue,
				maybe_generic => match (maybe_generic, try_unpack_generics(&comp.arguments)) {
					("Vec", Generics::Single(inner)) => TypeCategory::Vec(Box::new(parse_type(inner, false)?)),
					("Option", Generics::Single(inner)) => TypeCategory::Nullable(Box::new(parse_type(inner, false)?)),
					("HashMap", Generics::Double(syn::Type::Path(path), value)) if matches!(path.path.segments.last(), Some(last) if last.ident.to_string() == "String")
						=> TypeCategory::HashMap(Box::new(parse_type(value, false)?)),
					/*"Vec" if matches!(&comp.arguments, syn::PathArguments::AngleBracketed(args)
						if args.args.len() == 1 && matches!(args.args.first().unwrap(), syn::GenericArgument::Type(_))
					) => TypeCategory::Vec(Box::new(parse_type(match &comp.arguments {
						syn::PathArguments::AngleBracketed(args) => match args.args.first().unwrap() { syn::GenericArgument::Type(tp) => tp, _ => unreachable!() },
						_ => unreachable!()
					}, false)?)),
					"HashMap" if matches!(&comp.arguments, syn::PathArguments::AngleBracketed(args)
						if args.args.len() == 2 && matches!(args.args.first().unwrap(), syn::GenericArgument::Type(syn::Type::Path(string)) if matches!(
							string.path.segments.last(), Some(last) if last.ident.to_string().as_str() == "String"
						)) &&
						matches!(&args.args[1], syn::GenericArgument::Type(_))
					) => TypeCategory::HashMap(Box::new(parse_type(match &comp.arguments {
						syn::PathArguments::AngleBracketed(args) => match &args.args[1] { syn::GenericArgument::Type(tp) => tp, _ => unreachable!() },
						_ => unreachable!()
					}, false)?)),
					"Option" if matches!(&comp.arguments, syn::PathArguments::AngleBracketed(args)
						if args.args.len() == 1 && matches!(args.args.first().unwrap(), syn::GenericArgument::Type(_))
					) => TypeCategory::Nullable(Box::new(parse_type(match &comp.arguments {
						syn::PathArguments::AngleBracketed(args) => match args.args.first().unwrap() { syn::GenericArgument::Type(tp) => tp, _ => unreachable!() },
						_ => unreachable!()
					}, false)?)),*/
					_ => TypeCategory::Parsed(syn::Type::Path(path.clone())),
				}
			})
		}
		_ => return_error!(typ.span() => "Invalid member type"),
	}
}
