use syn::Result;
use super::*;
use crate::path::{JsonKey, ObjectKey, ArrayIndex};

pub fn gen_json_path(key: &JsonKey, typ: &syn::Type) -> Result<ParseCommands> {
	let mut comms = ParseCommands::new();

	let mut first = true;
	for path in &key.path.as_ref().expect("Someone forgot to assign the value!").inner {
		let step = match path {
			ObjectKey(key) => ParseStep::ObjIdx(key.as_str()),
			ArrayIndex(idx) => ParseStep::ArrIdx(*idx),
		};
		if first {
			first = false;
			comms.then(step);
		} else {
			let mut inner = ParseCommands::new();
			inner.then(step);
			comms.then(ParseStep::AndThen(inner));
		}
	}
	let typ = parse_type(&typ, key.allow_missing)?;
	if let TypeCategory::Option(inner) = typ {
		let mut body = ParseCommands::new();
		handle_type(&mut body, *inner);
		comms.then(ParseStep::MissingOr(body));
		comms.propagate();
	} else {
		comms.then(ParseStep::Require);
		comms.propagate();
		handle_type(&mut comms, typ);
	}
	Ok(comms)
}

fn handle_type(comms: &mut ParseCommands, typ: TypeCategory) {
	macro_rules! expand_def {
		{$dol:tt $(
			[$($pat:tt)+] => {
				$($exp: tt)+
			};
		)*} => {
			macro_rules! expand {
				[] => {};
				$(
					[$($pat)+ $dol($dol t:tt)*] => {{
						$($exp)+;
						expand!($dol($dol t)*);
					}};
				)*
			}
		};
	}

	expand_def! { $
		[AsStr] => { comms.then(ParseStep::As("str")) };
		[AsBool] => { comms.then(ParseStep::As("bool")) };
		[Into] => { comms.then(ParseStep::Into) };
		[Clone] => { comms.then(ParseStep::Clone) };
		[Parse $typ:ident] => { comms.then(ParseStep::As($typ.as_str())) };
		[?] => { comms.propagate() };
		[!] => { comms.then(ParseStep::Require) };
		[+ $inner:ident] => { handle_type(comms, *$inner) };
	}

	match typ {
		TypeCategory::String => expand![AsStr !? Into],
		TypeCategory::JsonValue => expand![Clone],
		TypeCategory::Bool => expand![AsBool !?],
		TypeCategory::Option(_) => unreachable!(),
		TypeCategory::Nullable(inner) => {
			let mut steps = ParseCommands::new();
			handle_type(&mut steps, *inner);
			comms.then(ParseStep::NullOr(steps));
			comms.propagate();
		}
		TypeCategory::Int(typ) => expand![Parse typ !?],
		TypeCategory::Float(typ) => expand![Parse typ !?],
		TypeCategory::Vec(inner) => {
			let mut steps = ParseCommands::new();
			handle_type(&mut steps, *inner);
			comms.then(ParseStep::Collect(steps));
			comms.propagate();
		}
		TypeCategory::HashMap(inner) => {
			let mut steps = ParseCommands::new();
			handle_type(&mut steps, *inner);
			comms.then(ParseStep::CollectMap(steps));
			comms.propagate();
		}
		TypeCategory::Parsed(typ) => {
			comms.then(ParseStep::RecParse(typ));
			comms.propagate();
		}
	}
}
