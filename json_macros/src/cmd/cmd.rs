//! Version 1 working with Option<T> as parse results

use crate::path::{JsonKey, ObjectKey, ArrayIndex};

use syn::Result;
use quote::{quote, ToTokens, TokenStreamExt};
use pm2::{Span, TokenStream, TokenTree};

#[derive(Debug)]
pub struct ParseCommands {
	steps: Vec<ParseStep>,
	last_propagate: Option<usize>,
	//#[cfg(debug_assertions)]
	//signature: TypeFlowSignature,
	//#[cfg(debug_assertions)]
	//last_propagate_type: Option<TypeSignature>,
}

/*#[cfg(debug_assertions)]
#[derive(Debug, PartialEq, Eq)]
struct TypeFlowSignature {
	from: TypeSignature,
	to: TypeSignature,
}

// Signature reduction
// (A -> B) (B -> C) => A -> C
// (A -> B?) ? (B -> C?) => (A -> B?) AndThen(B -> C?) : A -> C?
// (A -> B?) ? (B -> C) => (A -> B?) Map(B -> C) : A -> C?

#[cfg(debug_assertions)]
#[derive(Debug, Copy, Clone, PartialEq, Eq)]
enum TypeSignature {
	Json,
	Value,
	//Any,
	Vec,
	NullableValue,
	//NullableAny,
	MaybeJson,
	MaybeValue,
	//MaybeAny,
	MaybeVec,
	MaybeNullableValue,
	//MaybeNullableAny,
}

#[cfg(debug_assertions)]
impl TypeSignature {
	fn unwrap(self) -> Self {
		use self::TypeSignature::*;
		match self {
			MaybeJson => Json,
			MaybeValue => Value,
			MaybeVec => Vec,
			MaybeNullableValue => NullableValue,
			_ => panic!(),
		}
	}

	fn wrap(self) -> Self {
		use self::TypeSignature::*;
		match self {
			Json => MaybeJson,
			Value => MaybeValue,
			Vec => MaybeVec,
			NullableValue => MaybeNullableValue,
			_ => panic!(),
		}
	}
}*/

#[derive(Debug)]
pub enum ParseStep {
	// ? means maybe-error
	// % means maybe-null
	ObjIdx(*const str), // Json -> Json?
	ArrIdx(usize), // Json -> Json?
	As(&'static str), // Json -> Value?
	Into, // Value -> Value
	Clone, // Value -> Value
	AndThen(ParseCommands), // T? (T -> T?) -> T?
	NullOr(ParseCommands), // Json (Json -> T?) -> T%?
	Collect(ParseCommands), // Json (Json -> Value?) -> Vec?
	RecParse(syn::Type), // Json -> Value?
}

/*#[cfg(debug_assertions)]
impl ParseStep {
	fn signature(&self) -> TypeFlowSignature {
		use self::TypeSignature::*;
		use self::TypeFlowSignature as Sig;
		match self {
			ParseStep::ObjIdx(_) => Sig { from: Json, to: MaybeJson },
			ParseStep::ArrIdx(_) => Sig { from: Json, to: MaybeJson },
			ParseStep::As(_) => Sig { from: Json, to: MaybeValue },
			ParseStep::Into => Sig { from: Value, to: Value },
			ParseStep::Clone => Sig { from: Value, to: Value },
			ParseStep::AndThen(comm) => {
				assert!(
					match (comm.signature.from, comm.signature.to) {
						(Json, MaybeJson) => true,
						(Value, MaybeValue) => true,
						(NullableValue, MaybeNullableValue) => true,
						//(NullableAny, MaybeNullableAny) => true,
						(Vec, MaybeVec) => true,
						_ => false,
					}
				);
				Sig { from: comm.signature.to, to: comm.signature.to }
			}
			ParseStep::NullOr(comm) => {
				assert_eq!(comm.signature.from, Json);
				Sig {
					from: Json,
					to: match comm.signature.to {
						MaybeValue => MaybeNullableValue,
						_ => panic!(),
					}
				}
			}
			ParseStep::Collect(comm) => {
				assert_eq!(comm.signature.from, Json);
				Sig {
					from: Json,
					to: match comm.signature.to {
						MaybeJson => MaybeVec,
						MaybeValue => MaybeVec,
						_ => panic!(),
					}
				}
			}
			ParseStep::RecParse(_) => Sig { from: Json, to: MaybeValue },
		}
	}
}*/

impl ParseCommands {
	//#[cfg(not(debug_assertions))]
	pub fn new() -> Self {
		ParseCommands { steps: Vec::new(), last_propagate: None }
	}

	/*#[cfg(debug_assertions)]
	pub fn new() -> Self {
		ParseCommands { steps: Vec::new(), last_propagate: None, signature: TypeFlowSignature { from: TypeSignature::Json, to: TypeSignature::Json }, last_propagate_type: None }
	}*/

	/*pub fn then(&mut self, step: ParseStep) {
		let sig = step.signature();
		assert_eq!(self.signature.to, sig.from);
		self.signature.to = sig.to;
		self.steps.push(step);
	}*/

	pub fn then(&mut self, step: ParseStep) {
		self.steps.push(step);
	}

	//#[cfg(not(debug_assertions))]
	pub fn propagate(&mut self) {
		if let Some(prop) = self.last_propagate.take() {
			let splice: Vec<ParseStep> = self.steps.splice(prop.., std::iter::empty()).collect();
			self.then(ParseStep::AndThen(
				ParseCommands { steps: splice, last_propagate: None }
			));
		}
		self.last_propagate = Some(self.steps.len());
	}

	/*#[cfg(debug_assertions)]
	pub fn propagate(&mut self) {
		if let Some(prop) = self.last_propagate.take() {
			let prop_type = self.last_propagate_type.unwrap();
			let splice: Vec<ParseStep> = self.steps.splice(prop.., std::iter::empty()).collect();
			self.then(ParseStep::AndThen(
				ParseCommands { steps: splice, last_propagate: None, last_propagate_type: None, signature: TypeFlowSignature { from: prop_type.unwrap(), to: self.signature.to } }
			));
		}
		self.last_propagate_type = Some(self.signature.to);
		self.last_propagate = Some(self.steps.len());
		self.signature.to = self.signature.to.unwrap();
	}*/

	//#[cfg(not(debug_assertions))]
	pub fn unpropagate(&mut self) {
		/*match self.last_propagate.take() {
			Some(prop) => assert_eq!(prop, self.steps.len(), "Propagation defined at an earlier point"),
			None => panic!("Propagation undefined"),
		}*/
		match self.last_propagate.take() {
			Some(prop) if prop == self.steps.len() => (),
			Some(prop) => {
				// Manually propagate earlier steps
				let splice: Vec<ParseStep> = self.steps.splice(prop.., std::iter::empty()).collect();
				self.then(ParseStep::AndThen(ParseCommands { steps: splice, last_propagate: None }));
			}
			None => panic!("Propagation undefined"),
		}
	}

	/*#[cfg(debug_assertions)]
	pub fn unpropagate(&mut self) {
		match self.last_propagate.take() {
			Some(prop) if prop == self.steps.len() => {
				self.signature.to = self.last_propagate_type.unwrap();
			}
			Some(prop) => {
				let prop_type = self.last_propagate_type.unwrap();
				let to_type = self.signature.to.wrap();
				// Manually propagate earlier steps
				let splice: Vec<ParseStep> = self.steps.splice(prop.., std::iter::empty()).collect();
				self.then(ParseStep::AndThen(ParseCommands { steps: splice, last_propagate: None, last_propagate_type: None, signature:
					TypeFlowSignature { from: prop_type.unwrap(), to: to_type }
				}));
				self.signature.to = to_type;
			}
			None => panic!("Propagation undefined"),
		}
	}*/
}

impl ParseCommands {
	pub fn join_token_stream(&self, stream: &mut TokenStream) {
		/*#[cfg(debug_assertions)]
		{
			assert_eq!(self.signature.from, TypeSignature::Json);
			assert!(matches!(self.signature.to,
				TypeSignature::MaybeValue | TypeSignature::MaybeJson | TypeSignature::MaybeNullableValue | TypeSignature::MaybeVec
			));
		}*/
		for (idx, step) in self.steps.iter().enumerate() {
			if matches!(self.last_propagate, Some(i) if i == idx) {
				stream.append(pm2::Punct::new('?', pm2::Spacing::Alone))
			}
			match step {
				ParseStep::ObjIdx(key) => {
					stream.append(pm2::Punct::new('.', pm2::Spacing::Alone));
					// TODO: Improve span
					stream.append(pm2::Ident::new("safe_index_obj", Span::call_site()));
					stream.append(pm2::Group::new(pm2::Delimiter::Parenthesis, TokenTree::Literal(pm2::Literal::string(unsafe{&**key})).into_token_stream()));
				}
				ParseStep::ArrIdx(idx) => {
					stream.append(pm2::Punct::new('.', pm2::Spacing::Alone));
					// TODO: Improve span
					stream.append(pm2::Ident::new("safe_index_arr", Span::call_site()));
					stream.append(pm2::Group::new(pm2::Delimiter::Parenthesis, TokenTree::Literal(pm2::Literal::usize_unsuffixed(*idx)).into_token_stream()));
				}
				ParseStep::As(typ) => {
					stream.append(pm2::Punct::new('.', pm2::Spacing::Alone));
					// TODO: Improve span
					stream.append(quote::format_ident!("as_{typ}"));
					stream.append(pm2::Group::new(pm2::Delimiter::Parenthesis, TokenStream::new()));
				}
				ParseStep::Into => {
					stream.append(pm2::Punct::new('.', pm2::Spacing::Alone));
					// TODO: Improve span
					stream.append(pm2::Ident::new("into", Span::call_site()));
					stream.append(pm2::Group::new(pm2::Delimiter::Parenthesis, TokenStream::new()));
				}
				ParseStep::Clone => {
					stream.append(pm2::Punct::new('.', pm2::Spacing::Alone));
					// TODO: Improve span
					stream.append(pm2::Ident::new("clone", Span::call_site()));
					stream.append(pm2::Group::new(pm2::Delimiter::Parenthesis, TokenStream::new()));
				}
				ParseStep::AndThen(comm) => {
					stream.append(pm2::Punct::new('.', pm2::Spacing::Alone));
					// TODO: Improve span
					stream.append(pm2::Ident::new("and_then", Span::call_site()));
					stream.append(pm2::Group::new(pm2::Delimiter::Parenthesis, {
						let mut subs = pm2::TokenStream::new();
						subs.append(pm2::Ident::new("cont", pm2::Span::call_site()));
						comm.join_token_stream(&mut subs);
						quote!(|cont| #subs)
					}));
				}
				ParseStep::NullOr(comm) => {
					stream.append(pm2::Punct::new('.', pm2::Spacing::Alone));
					// TODO: Improve span
					stream.append(pm2::Ident::new("null_or", Span::call_site()));
					stream.append(pm2::Group::new(pm2::Delimiter::Parenthesis, {
						let mut subs = pm2::TokenStream::new();
						subs.append(pm2::Ident::new("cont", pm2::Span::call_site()));
						comm.join_token_stream(&mut subs);
						quote!(|cont| Some(#subs))
					}));
				}
				ParseStep::Collect(comm) => {
					stream.append(pm2::Punct::new('.', pm2::Spacing::Alone));
					// TODO: Improve span
					stream.append(pm2::Ident::new("collect_arr", Span::call_site()));
					stream.append(pm2::Group::new(pm2::Delimiter::Parenthesis, {
						let mut subs = pm2::TokenStream::new();
						subs.append(pm2::Ident::new("item", pm2::Span::call_site()));
						comm.join_token_stream(&mut subs);
						quote!(|item| #subs)
					}));
				}
				ParseStep::RecParse(typ) => {
					let mut new = TokenStream::new();
					new.append(pm2::Punct::new('<', pm2::Spacing::Alone));
					typ.to_tokens(&mut new);
					new.append_all([
						pm2::TokenTree::Punct(pm2::Punct::new('>', pm2::Spacing::Alone)),
						pm2::TokenTree::Punct(pm2::Punct::new(':', pm2::Spacing::Joint)),
						pm2::TokenTree::Punct(pm2::Punct::new(':', pm2::Spacing::Alone)),
						pm2::TokenTree::Ident(pm2::Ident::new("parse", pm2::Span::call_site())),
						pm2::TokenTree::Group(pm2::Group::new(pm2::Delimiter::Parenthesis, std::mem::take(stream))),
					]);
					*stream = new;
				}
			}
		}
		if matches!(self.last_propagate, Some(i) if i == self.steps.len()) {
			stream.append(pm2::Punct::new('?', pm2::Spacing::Alone))
		}
	}
}
