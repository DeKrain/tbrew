//! Version 2 working with json::Result<T> as parse results

use quote::{quote, ToTokens, TokenStreamExt};
use pm2::{Span, TokenStream, TokenTree};

#[derive(Debug)]
pub struct ParseCommands {
	steps: Vec<ParseStep>,
}

#[derive(Debug)]
pub enum ParseStep {
	// ? means maybe-error
	// % means maybe-nothing
	ObjIdx(*const str), // Json -> Json%
	ArrIdx(usize), // Json -> Json%
	Require, // T% -> T?
	As(&'static str), // Json -> Value?
	Into, // Value -> Value
	Clone, // Value -> Value
	Propagate, // T? ->* T
	AndThen(ParseCommands), // T? (T -> T?) -> T?
	NullOr(ParseCommands), // Json (Json -> T?) -> T%?
	MissingOr(ParseCommands), // Json% (Json -> T?) -> T%?
	Collect(ParseCommands), // Json (Json -> Value?) -> Vec?
	CollectMap(ParseCommands), // Json (Json -> Value?) -> HashMap?
	RecParse(syn::Type), // Json -> Value?
}

impl ParseCommands {
	pub fn new() -> Self {
		ParseCommands { steps: Vec::new() }
	}

	pub fn then(&mut self, step: ParseStep) {
		self.steps.push(step);
	}

	pub fn propagate(&mut self) {
		self.then(ParseStep::Propagate);
	}

	pub fn join_token_stream(&self, stream: &mut TokenStream) {
		self.join_token_stream_inner(stream, false)
	}

	pub fn join_token_stream_inner(&self, stream: &mut TokenStream, omit_last_prop: bool) {
		let mut needs_prop = false;
		for step in self.steps.iter() {
			if needs_prop {
				needs_prop = false;
				stream.append(pm2::Punct::new('?', pm2::Spacing::Alone));
			}
			match *step {
				ParseStep::ObjIdx(key) => stream.extend([
					TokenTree::Punct(pm2::Punct::new('.', pm2::Spacing::Alone)),
					// TODO: Improve span
					TokenTree::Ident(pm2::Ident::new("safe_index_obj", Span::call_site())),
					TokenTree::Group(pm2::Group::new(pm2::Delimiter::Parenthesis, TokenTree::Literal(pm2::Literal::string(unsafe{&*key})).into_token_stream())),
				]),
				ParseStep::ArrIdx(idx) => stream.extend([
					TokenTree::Punct(pm2::Punct::new('.', pm2::Spacing::Alone)),
					// TODO: Improve span
					TokenTree::Ident(pm2::Ident::new("safe_index_arr", Span::call_site())),
					TokenTree::Group(pm2::Group::new(pm2::Delimiter::Parenthesis, TokenTree::Literal(pm2::Literal::usize_unsuffixed(idx)).into_token_stream())),
				]),
				ParseStep::Require => stream.extend(quote!(.ok_or(json::ParseError))),
				ParseStep::As(typ) => stream.extend([
					TokenTree::Punct(pm2::Punct::new('.', pm2::Spacing::Alone)),
					// TODO: Improve span
					TokenTree::Ident(quote::format_ident!("as_{typ}")),
					TokenTree::Group(pm2::Group::new(pm2::Delimiter::Parenthesis, TokenStream::new())),
				]),
				ParseStep::Into => stream.extend([
					TokenTree::Punct(pm2::Punct::new('.', pm2::Spacing::Alone)),
					// TODO: Improve span
					TokenTree::Ident(pm2::Ident::new("into", Span::call_site())),
					TokenTree::Group(pm2::Group::new(pm2::Delimiter::Parenthesis, TokenStream::new())),
				]),
				ParseStep::Clone => stream.extend([
					TokenTree::Punct(pm2::Punct::new('.', pm2::Spacing::Alone)),
					// TODO: Improve span
					TokenTree::Ident(pm2::Ident::new("clone", Span::call_site())),
					TokenTree::Group(pm2::Group::new(pm2::Delimiter::Parenthesis, TokenStream::new())),
				]),
				ParseStep::Propagate => {
					// Delay to next step
					needs_prop = true;
				}
				ParseStep::AndThen(ref comm) => stream.extend([
					TokenTree::Punct(pm2::Punct::new('.', pm2::Spacing::Alone)),
					// TODO: Improve span
					TokenTree::Ident(pm2::Ident::new("and_then", Span::call_site())),
					TokenTree::Group(pm2::Group::new(pm2::Delimiter::Parenthesis, {
						let mut subs = pm2::TokenStream::new();
						subs.append(pm2::Ident::new("cont", pm2::Span::call_site()));
						comm.join_token_stream_inner(&mut subs, false);
						quote!(|cont| #subs)
					})),
				]),
				ParseStep::NullOr(ref comm) => stream.extend([
					TokenTree::Punct(pm2::Punct::new('.', pm2::Spacing::Alone)),
					// TODO: Improve span
					TokenTree::Ident(pm2::Ident::new("null_or", Span::call_site())),
					TokenTree::Group(pm2::Group::new(pm2::Delimiter::Parenthesis, {
						let mut subs = pm2::TokenStream::new();
						subs.append(pm2::Ident::new("cont", pm2::Span::call_site()));
						if matches!(comm.steps.last(), Some(ParseStep::Propagate)) {
							comm.join_token_stream_inner(&mut subs, true);
							quote!(|cont| #subs)
						} else {
							comm.join_token_stream_inner(&mut subs, false);
							quote!(|cont| Ok(#subs))
						}
					})),
				]),
				ParseStep::MissingOr(ref comm) => stream.extend([
					TokenTree::Punct(pm2::Punct::new('.', pm2::Spacing::Alone)),
					// TODO: Improve span
					TokenTree::Ident(pm2::Ident::new("missing_or", Span::call_site())),
					TokenTree::Group(pm2::Group::new(pm2::Delimiter::Parenthesis, {
						let mut subs = pm2::TokenStream::new();
						subs.append(pm2::Ident::new("cont", pm2::Span::call_site()));
						if matches!(comm.steps.last(), Some(ParseStep::Propagate)) {
							comm.join_token_stream_inner(&mut subs, true);
							quote!(|cont| #subs)
						} else {
							comm.join_token_stream_inner(&mut subs, false);
							quote!(|cont| Ok(#subs))
						}
					})),
				]),
				ParseStep::Collect(ref comm) => stream.extend([
					TokenTree::Punct(pm2::Punct::new('.', pm2::Spacing::Alone)),
					// TODO: Improve span
					TokenTree::Ident(pm2::Ident::new("collect_arr", Span::call_site())),
					TokenTree::Group(pm2::Group::new(pm2::Delimiter::Parenthesis, {
						let mut subs = pm2::TokenStream::new();
						subs.append(pm2::Ident::new("item", pm2::Span::call_site()));
						if matches!(comm.steps.last(), Some(ParseStep::Propagate)) {
							comm.join_token_stream_inner(&mut subs, true);
							quote!(|item| #subs)
						} else {
							comm.join_token_stream_inner(&mut subs, false);
							quote!(|item| Ok(#subs))
						}
					})),
				]),
				ParseStep::CollectMap(ref comm) => stream.extend([
					TokenTree::Punct(pm2::Punct::new('.', pm2::Spacing::Alone)),
					// TODO: Improve span
					TokenTree::Ident(pm2::Ident::new("collect_obj", Span::call_site())),
					TokenTree::Group(pm2::Group::new(pm2::Delimiter::Parenthesis, {
						let mut subs = pm2::TokenStream::new();
						subs.append(pm2::Ident::new("item", pm2::Span::call_site()));
						if matches!(comm.steps.last(), Some(ParseStep::Propagate)) {
							comm.join_token_stream_inner(&mut subs, true);
							quote!(|item| #subs)
						} else {
							comm.join_token_stream_inner(&mut subs, false);
							quote!(|item| Ok(#subs))
						}
					})),
				]),
				ParseStep::RecParse(ref typ) => {
					let mut new = TokenStream::new();
					new.append(pm2::Punct::new('<', pm2::Spacing::Alone));
					typ.to_tokens(&mut new);
					new.extend([
						pm2::TokenTree::Ident(pm2::Ident::new("as", pm2::Span::call_site())),
						pm2::TokenTree::Ident(pm2::Ident::new("json", pm2::Span::call_site())),
						pm2::TokenTree::Punct(pm2::Punct::new(':', pm2::Spacing::Joint)),
						pm2::TokenTree::Punct(pm2::Punct::new(':', pm2::Spacing::Alone)),
						pm2::TokenTree::Ident(pm2::Ident::new("JsonParse", pm2::Span::call_site())),
						pm2::TokenTree::Punct(pm2::Punct::new('>', pm2::Spacing::Alone)),
						pm2::TokenTree::Punct(pm2::Punct::new(':', pm2::Spacing::Joint)),
						pm2::TokenTree::Punct(pm2::Punct::new(':', pm2::Spacing::Alone)),
						pm2::TokenTree::Ident(pm2::Ident::new("parse", pm2::Span::call_site())),
						pm2::TokenTree::Group(pm2::Group::new(pm2::Delimiter::Parenthesis, std::mem::take(stream))),
					]);
					*stream = new;
				}
			}
		}
		if needs_prop && !omit_last_prop {
			stream.append(pm2::Punct::new('?', pm2::Spacing::Alone));
		}
	}
}
