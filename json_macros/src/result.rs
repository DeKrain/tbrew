pub struct StaticError(pub &'static str);

pub type SResult<T> = std::result::Result<T, StaticError>;
pub type DynResult<T> = std::result::Result<T, Box<dyn std::error::Error>>;

impl std::error::Error for StaticError {}

impl std::fmt::Display for StaticError {
	fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
		f.write_str(self.0)
	}
}

impl std::fmt::Debug for StaticError {
	fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
		f.write_str(self.0)
	}
}

macro_rules! report_error {
	($span: expr => $msg: expr) => {
		return Error::new($span, $msg).into_compile_error().into()
	};
}

macro_rules! return_error {
	($span: expr => $msg: expr) => {
		return Err(syn::Error::new($span, $msg))
	};
}

macro_rules! tt_unwrap {
	($expr: expr) => {
		match $expr {
			Ok(ok) => ok,
			Err(err) => return err.into_compile_error().into(),
		}
	}
}

macro_rules! se_unwrap {
	($span: expr => $expr: expr) => {
		match $expr {
			Ok(ok) => ok,
			Err(err) => return Error::new($span, err.0).into_compile_error().into(),
		}
	}
}
