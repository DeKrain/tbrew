use syn::Token;

pub enum PathComponent {
	ObjectKey(String),
	/// End-relative indices are not supported yet
	ArrayIndex(usize),
}

pub use PathComponent::*;

pub struct PathParseError<'span> {
	msg: &'static str,
	span: &'span str,
}

type ParseResult<'span, T> = Result<T, PathParseError<'span>>;

macro_rules! mkerr {
	($span:ident $msg:literal) => {
		Err(PathParseError { msg: $msg, span: $span })
	}
}

/// Represents a JSON tree path, like foo.0.baz."my-str". /* digit string key */"123"
pub struct Path {
	pub inner: Vec<PathComponent>,
}

impl Path {
	pub fn new_key(ident: String) -> Self {
		Path { inner: vec![ObjectKey(ident)] }
	}

	pub fn new_idx(index: usize) -> Self {
		Path { inner: vec![ArrayIndex(index)] }
	}

	pub fn parse(mut path: &str) -> ParseResult<Self> {
		use self::parse_str::{byte, parse_string_literal};

		let mut res = Path { inner: Vec::new() };
		if path.is_empty() {
			return Ok(res);
		}
		loop {
			match byte(path, 0) {
				b'0'..=b'9' => {
					let dot = path.find('.');
					let key;
					(key, path) = match dot { Some(dot) => { path.split_at(dot) } None => (path, "") };
					let key: usize = match key.parse() {
						Ok(key) => key,
						Err(_) => return mkerr!(key "Invalid array index"),
					};
					res.inner.push(ArrayIndex(key));
				}
				b'_' | b'-' | b'A'..=b'Z' | b'a'..=b'z' => {
					let dot = path.find('.');
					let key;
					(key, path) = match dot { Some(dot) => path.split_at(dot), None => (path, "") };
					for &x in key.as_bytes() {
						if x != b'_' && x != b'-' && !x.is_ascii_alphanumeric() {
							//return Err(Box::new(StaticError("Identifier path must consist only of identifier characters")));
							return mkerr!(key "Identifier path must consist only of identifier characters");
						}
					}
					res.inner.push(ObjectKey(key.to_string()));
				}
				b'"' => {
					let key;
					(key, path) = parse_string_literal(path)?;
					if !path.is_empty() && !path.starts_with('.') {
						return mkerr!(path "Period must follow a literal path");
					}
					res.inner.push(ObjectKey(key));
				},
				_ => return mkerr!(path "Invalid path character"),
			}

			if path.is_empty() {
				break;
			}

			assert!(byte(path, 0) == b'.');
			path = &path[1..];
			if path.is_empty() {
				return mkerr!(path "Expected a path component after period");
			}
		}
		Ok(res)
	}
}

pub struct JsonKey {
	pub path: Option<Path>,
	/// If true, the parsed value is Some(v) if the key is present, otherwise None
	pub allow_missing: bool,
}

impl JsonKey {
	pub fn new() -> Self {
		JsonKey { path: None, allow_missing: false }
	}
}

impl syn::parse::Parse for JsonKey {
	fn parse(input: syn::parse::ParseStream) -> syn::Result<JsonKey> {
		let mut key = JsonKey { path: None, allow_missing: false };
		loop {
			if input.is_empty() {
				break;
			}
			let span = input.span();
			if let Ok(syn::Lit::Str(lit)) = input.parse() {
				if key.path.is_some() {
					return_error!(lit.span() => "Cannot have multiple paths");
				}
				let str = lit.value();
				let path = Path::parse(str.as_str());
				let path = match path {
					Ok(ok) => ok,
					Err(err) => return_error!(lit.span() => err.msg),
				};
				if !path.inner.is_empty() {
					key.path = Some(path);
				}
			} else {
				match input.parse::<syn::Ident>() {
					Ok(id) if id == "allow_missing" => {
						key.allow_missing = true;
					}
					_ => return_error!(span => "Invalid item"),
				}
			}
			/*input.step(|cursor| {
				let lit = match cursor.token_tree() {
					TokenTree::Literal()
					Some((lit, _)) => match lit.into() as syn::Lit {
						syn::Lit::Str(lit) => lit,
						_ => None,
					},
					_ => None,
				}?;
				key.path = JsonPath::parse();
				Ok(())
			})?;*/
			if input.is_empty() {
				break;
			}
			<Token![,]>::parse(input)?;
		}
		Ok(key)
	}
}

mod parse_str {
	//! Yoinked from syn::lit

	use super::{ParseResult, PathParseError};

	pub fn next_chr(s: &str) -> char {
		s.chars().next().unwrap_or('\0')
	}
	
	pub fn parse_string_literal(mut s: &str) -> ParseResult<(String, &str)> {
		assert_eq!(byte(s, 0), b'"');
		s = &s[1..];
	
		let mut content = String::new();
		'outer: loop {
			let ch = match byte(s, 0) {
				b'"' => break,
				b'\\' => {
					let b = byte(s, 1);
					let bs = &s[1..=1];
					s = &s[2..];
					match b {
						b'x' => {
							let (byte, rest) = backslash_x(s)?;
							s = rest;
							if byte > 0x80 {
								return mkerr!(s "Invalid \\x byte in string literal");
							}
							// Value in range
							char::from_u32(u32::from(byte)).unwrap()
						}
						b'u' => {
							let (chr, rest) = backslash_u(s)?;
							s = rest;
							chr
						}
						b'n' => '\n',
						b'r' => '\r',
						b't' => '\t',
						b'\\' => '\\',
						b'0' => '\0',
						b'\'' => '\'',
						b'"' => '"',
						b'\r' | b'\n' => loop {
							let ch = next_chr(s);
							if ch.is_whitespace() {
								s = &s[ch.len_utf8()..];
							} else {
								continue 'outer;
							}
						},
						//b => return Err(format!("Unexpected byte {:?} after \\ character in byte literal", b)),
						_ => return mkerr!(bs "Unexpected byte after \\ character in byte literal"),
					}
				}
				b'\r' => {
					if byte(s, 1) != b'\n' {
						return mkerr!(s "Bare CR not allowed in string");
					}
					s = &s[2..];
					'\n'
				}
				_ => {
					let ch = next_chr(s);
					s = &s[ch.len_utf8()..];
					ch
				}
			};
			content.push(ch);
		}
	
		if !s.starts_with('"') {
			mkerr!(s "String literal must be terminated")
		} else {
			Ok((content, &s[1..]))
		}
	}
	
	/// Get the byte at offset idx, or a default of `b'\0'` if we're looking
	/// past the end of the input buffer.
	pub fn byte<S: AsRef<[u8]> + ?Sized>(s: &S, idx: usize) -> u8 {
		let s = s.as_ref();
		if idx < s.len() {
			s[idx]
		} else {
			0
		}
	}
	
	fn backslash_x(s: &str) -> ParseResult<(u8, &str)>
	//where
	//	S: Index<RangeFrom<usize>, Output = S> + AsRef<[u8]> + ?Sized,
	{
		let mut ch = 0;
		let b0 = byte(s, 0);
		let b1 = byte(s, 1);
		ch += 0x10
			* match b0 {
				b'0'..=b'9' => b0 - b'0',
				b'a'..=b'f' => 10 + (b0 - b'a'),
				b'A'..=b'F' => 10 + (b0 - b'A'),
				_ => return mkerr!(s "Unexpected non-hex character after \\x"),
			};
		ch += match b1 {
			b'0'..=b'9' => b1 - b'0',
			b'a'..=b'f' => 10 + (b1 - b'a'),
			b'A'..=b'F' => 10 + (b1 - b'A'),
			_ => return mkerr!(s "Unexpected non-hex character after \\x"),
		};
		Ok((ch, &s[2..]))
	}
	
	fn backslash_u(mut s: &str) -> ParseResult<(char, &str)> {
		if byte(s, 0) != b'{' {
			return mkerr!(s "Expected { after \\u");
		}
		s = &s[1..];
	
		let mut ch = 0;
		let mut digits = 0;
		loop {
			let b = byte(s, 0);
			let digit = match b {
				b'0'..=b'9' => b - b'0',
				b'a'..=b'f' => 10 + b - b'a',
				b'A'..=b'F' => 10 + b - b'A',
				b'_' if digits > 0 => {
					s = &s[1..];
					continue;
				}
				b'}' if digits == 0 => return mkerr!(s "Invalid empty unicode escape"),
				b'}' => break,
				_ => return mkerr!(s "Unexpected non-hex character after \\u"),
			};
			if digits == 6 {
				return mkerr!(s "Overlong unicode escape (must have at most 6 hex digits)");
			}
			ch *= 0x10;
			ch += u32::from(digit);
			digits += 1;
			s = &s[1..];
		}
		if byte(s, 0) != b'}' {
			return mkerr!(s "Expected '}' after \\u{...");
		}
		s = &s[1..];
	
		if let Some(ch) = char::from_u32(ch) {
			Ok((ch, s))
		} else {
			//Err(StaticError("Character code {:x} is not a valid unicode character", ch))
			mkerr!(s "Character code is not a valid unicode character")
		}
	}
}
