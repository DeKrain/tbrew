use {
	crate::{
		utils,
		utils::loggers::*,
		json::{self, JsonExtensions, JsonParse, JsonOptionExt},
		json_traverse_safe,
	},
	json_macros::JsonParse,
	std::{
		fs,
		collections::HashMap,
		path::{Path, PathBuf},
		ffi::OsString,
	},
};

pub struct McLauncher {
	config: LauncherConfig,
	parameters: HashMap<String, String>,
	props: json::JsonValue,
	game_args: Vec<String>,
	jvm_args: Vec<String>,
}

pub struct LauncherConfig {
	pub launcher: &'static str,
	pub arch: Arch,
	pub platform: Platform,
	pub os: OSPlatform,
	pub silent: bool,
	pub player: String,
	pub gamedir: PathBuf,
	pub verdir: PathBuf,
	pub libdir: PathBuf,
	pub version: String,
	pub gamefile: PathBuf,
	pub auth: Option<Auth>,
	pub jvm_args: Vec<String>,
	pub game_args: Vec<String>,
	pub jvm_classpath: Vec<String>,
}

#[allow(non_camel_case_types)]
#[repr(u8)]
#[derive(Clone, Copy)]
pub enum Arch {
	x86_32,
	x86_64,
	arm,
	aarch64,
}

impl Arch {
	pub fn current() -> Arch {
		#[cfg(target_arch = "x86")]
		{
			Arch::x86_32
		}
		#[cfg(target_arch = "x86_64")]
		{
			Arch::x86_64
		}
		#[cfg(target_arch = "arm")]
		{
			Arch::arm
		}
		#[cfg(target_arch = "aarch64")]
		{
			Arch::aarch64
		}
		#[cfg(not(any(target_arch = "x86", target_arch = "x86_64", target_arch = "arm", target_arch = "aarch64")))]
		{
			panic!("Unsupported target architecture")
		}
	}
}

#[derive(JsonParse, Clone, Copy, PartialEq, Eq)]
pub enum OSPlatform {
	#[json_spelling = "linux"]
	Linux,

	#[json_spelling = "osx"]
	#[json_spelling = "macos"]
	#[allow(non_camel_case_types)]
	macOS,

	#[json_spelling = "windows"]
	Windows,

	#[json_spelling(exclude)]
	Unknown,
}

impl OSPlatform {
	pub fn current() -> OSPlatform {
		#[cfg(any(target_os = "linux", target_os = "android"))]
		{
			OSPlatform::Linux
		}
		#[cfg(any(target_os = "macos", target_os = "ios"))]
		{
			OSPlatform::macOS
		}
		#[cfg(target_os = "windows")]
		{
			OSPlatform::Windows
		}
		#[cfg(not(any(target_os = "linux", target_os = "android", target_os = "macos", target_os = "ios", target_os = "windows")))]
		{
			OSPlatform::Unknown
		}
	}

	pub fn native_index(self, map: &HashMap<String, LibraryArtifact>) -> Option<&LibraryArtifact> {
		match self {
			OSPlatform::Linux => map.get("linux"),
			OSPlatform::Windows => map.get("windows"),

			// For macOS, try macos first, then osx
			OSPlatform::macOS => map.get("macos").or_else(|| map.get("osx")),

			OSPlatform::Unknown => None,
		}
	}
}

#[derive(PartialEq)]
pub enum Platform {
	Desktop,
	Android,
}

pub struct Auth {
	pub uuid: UUID,
	pub token: String,
	pub online: bool,
}

pub union UUID {
	bytes: [u8; 16],
}

impl UUID {
	#[allow(non_upper_case_globals)]
	const hex_fmt: &[u8; 16] = b"0123456789ABCDEF";
	pub fn to_str(&self) -> String {
		let mut buf = [0u8; 2*16 + 4];
		unsafe {
			for i in 0..4 {
				buf[2*i + 0] = *Self::hex_fmt.get_unchecked((self.bytes[i] >> 4) as usize);
				buf[2*i + 1] = *Self::hex_fmt.get_unchecked((self.bytes[i] & 0xF) as usize);
			}
			buf[2*4 + 0] = b'-';
			for i in 4..6 {
				buf[2*i + 1] = *Self::hex_fmt.get_unchecked((self.bytes[i] >> 4) as usize);
				buf[2*i + 2] = *Self::hex_fmt.get_unchecked((self.bytes[i] & 0xF) as usize);
			}
			buf[2*6 + 1] = b'-';
			for i in 6..8 {
				buf[2*i + 2] = *Self::hex_fmt.get_unchecked((self.bytes[i] >> 4) as usize);
				buf[2*i + 3] = *Self::hex_fmt.get_unchecked((self.bytes[i] & 0xF) as usize);
			}
			buf[2*8 + 2] = b'-';
			for i in 8..10 {
				buf[2*i + 3] = *Self::hex_fmt.get_unchecked((self.bytes[i] >> 4) as usize);
				buf[2*i + 4] = *Self::hex_fmt.get_unchecked((self.bytes[i] & 0xF) as usize);
			}
			buf[2*10 + 3] = b'-';
			for i in 10..16 {
				buf[2*i + 4] = *Self::hex_fmt.get_unchecked((self.bytes[i] >> 4) as usize);
				buf[2*i + 5] = *Self::hex_fmt.get_unchecked((self.bytes[i] & 0xF) as usize);
			}
			String::from_utf8_unchecked(Vec::from(buf))
		}
	}
}

impl McLauncher {
	pub fn create(config: LauncherConfig) -> Self {
		let mut launcher = McLauncher {
			config,
			parameters: HashMap::new(),
			props: json::JsonValue::Null,
			game_args: Vec::new(),
			jvm_args: Vec::new(),
		};
		launcher.config.jvm_classpath.push(launcher.config.gamefile.as_os_str().to_str().unwrap().to_owned());
		launcher
	}

	fn init_env(&mut self) {
		macro_rules! insert {
			{
				$ref: expr =>
				$(
					$key: expr => $value: expr
				),+ $(,)?
			} => {
				{
					let ref mut map = &mut $ref;
					$(
						map.insert($key.into(), $value);
					)+
				}
			};
		}
		insert! { self.parameters =>
			"auth_player_name" => self.config.player.clone(),
			"version_name" => self.config.version.clone(),
			"game_directory" => self.config.gamedir.as_os_str().to_str().unwrap().to_owned(),
			"assets_root" => self.config.gamedir.join("assets").into_os_string().into_string().unwrap(),
			"assets_index_name" => self.props["assets"].as_str().unwrap().into(),
		};
		if let Some(ref auth) = self.config.auth {
			insert! { self.parameters =>
				"auth_uuid" => auth.uuid.to_str(),
				"auth_xuid" => auth.uuid.to_str(),
				"auth_access_token" => auth.token.clone(),
				"user_type" => if auth.online { "online" } else { "offline" }.into(),
			};
		} else {
			insert! { self.parameters =>
				"auth_uuid" => String::new(),
				"auth_xuid" => String::new(),
				"auth_access_token" => String::new(),
				"user_type" => "offline".into(),
			};
		}
		insert! { self.parameters =>
			"version_type" => self.props["type"].as_str().unwrap().into(),
			//"classpath" => ..., // Filled in later
			"natives_directory" => self.config.verdir.join("natives").into_os_string().into_string().unwrap(),
			"launcher_name" => self.config.launcher.into(),
			"launcher_version" => "0".into(),
			"clientid" => "0".into(),

			// Legacy params
			"auth_session" => self.config.auth.as_ref().map_or_else(|| String::new(), |auth| auth.uuid.to_str()),
		};
	}

	fn parse_arg(&self, arg: &str) -> String {
		let mut arg = arg.split("${");
		let mut out: String = arg.next().unwrap().into();
		for p in arg {
			let p = p.split_once('}').expect("version.json: unterminated substitution, expected '}' after '${'");
			let v = match self.parameters.get(p.0) {
				Some(v) => v,
				None => panic!("version.json: unrecognized parameter: {}", p.0),
			};
			out += v;
			out += p.1;
		}
		out
	}

	fn validate_rules(&self, rules: &[Rule]) -> bool {
		rules.iter().all(|rule| {
			(rule.os.as_ref().map_or(true,
				|os| os.arch.map_or(true, |arch| arch.check(self.config.arch))
				&& os.name.as_ref().map_or(true, |&name| name == self.config.os))
			&& rule.features.as_ref().map_or(true, |f| f.is_empty()) // ignore all features
			) ^ matches!(rule.action, RuleAction::Disallow)
		})
	}

	fn construct_args(&mut self) {
		self.parameters.insert("classpath".into(), self.config.jvm_classpath.join(":"));
		if let Some(arguments) = self.props.safe_index_obj("arguments") {
			self.game_args = self.construct_game_args(arguments.safe_index_obj("game").unwrap().iter_arr().unwrap().as_slice(), &self.config.game_args);
			self.jvm_args = self.construct_game_args(arguments.safe_index_obj("jvm").unwrap().iter_arr().unwrap().as_slice(), &self.config.jvm_args);
		} else {
			self.game_args = self.construct_game_args_str(self.props.safe_index_obj("minecraftArguments").unwrap().as_str().unwrap().split(' ').collect::<Vec<_>>().as_slice(), &self.config.game_args);
			self.jvm_args = self.construct_game_args_str(&["-cp", "${classpath}", "-Djava.library.path=${natives_directory}"], &self.config.jvm_args);
		}
	}

	fn construct_game_args_str(&self, args: &[&str], config: &[String]) -> Vec<String> {
		let mut list: Vec<_> = args.iter().map(|arg| self.parse_arg(arg)).collect();
		for arg in config {
			list.push(arg.clone());
		}
		list
	}

	fn construct_game_args(&self, args: &[json::JsonValue], config: &[String]) -> Vec<String> {
		let mut list = Vec::new();
		for arg in args {
			if let Some(str_arg) = arg.as_str() {
				list.push(self.parse_arg(str_arg));
			} else {
				if self.validate_rules(arg.safe_index_obj("rules").unwrap().collect_arr(|rule| Rule::parse(rule)).unwrap().as_slice()) {
					let value = arg.safe_index_obj("value").unwrap();
					if let Some(value) = value.as_str() {
						list.push(self.parse_arg(value));
					} else {
						for item in value.iter_arr().unwrap() {
							list.push(self.parse_arg(item.as_str().unwrap()));
						}
					}
				}
			}
		}
		for arg in config {
			list.push(arg.clone());
		}
		list
	}

	// Structure
	// /
	//	/versions
	//	/libraries
	//	/assets
	//		/assets/indexes
	//		/assets/objects
	pub fn ensure_gamedir(&self) {
		utils::ensure_path(&self.config.gamedir, Path::new("."));
		utils::ensure_path(&self.config.gamedir, Path::new("versions/."));
		utils::ensure_path(&self.config.gamedir, Path::new("libraries/."));
		utils::ensure_path(&self.config.gamedir, Path::new("assets/indexes/."));
		utils::ensure_path(&self.config.gamedir, Path::new("assets/objects/."));
	}

	pub fn download_libraries(&mut self) {
		debug_assert!(self.props.is_object());
		let spec = &self.props["libraries"];
		for library in spec.iter_arr().unwrap() {
			let library = match Library::parse(library) {
				Ok(ok) => ok,
				Err(_) => {
					log_err!("Wrong library structure: {}", library);
					panic!();
				}
			};
			if !self.validate_rules(&library.rules) {
				continue;
			}
			let extract = library.extract.as_ref();
			// Main artifact (Java library)
			if let Some(main_artifact) = &library.main_artifact {
				let fs_path = self.config.libdir.join(&main_artifact.path);
				self.config.jvm_classpath.push(fs_path.as_os_str().to_str().unwrap().to_owned());
				if !fs_path.exists() {
					log_action!("Downloading library: {}", library.name);
					utils::ensure_path(&self.config.libdir, &main_artifact.path);
					utils::download_file(&main_artifact.url, &fs_path);
					if extract.is_some() {
						log_info!("Non-native library needs extraction: {}", fs_path.display());
					}
				}
			}
			// Check if library name is duplicated (native libs only)
			//let is_native = self.downloaded_libraries.contains(&library.name);
			if let Some(native_artifact) = self.config.os.native_index(&library.native_artifacts) {
				let fs_path = self.config.libdir.join(&native_artifact.path);
				if !extract.is_some() {
					self.config.jvm_classpath.push(fs_path.as_os_str().to_str().unwrap().to_owned());
				}
				if !fs_path.exists() {
					log_action!("Downloading native library: {}", library.name);
					utils::ensure_path(&self.config.libdir, &native_artifact.path);
					utils::download_file(&native_artifact.url, &fs_path);
				}
				if let Some(extract) = extract {
					log_action!("Extracting native library: {}", fs_path.display());
					utils::extract_zip(&fs_path, &self.config.verdir.join("natives"), &extract.exclude);
				}
			}
		}
	}

	pub fn download_assets(&mut self) {
		debug_assert!(self.props.is_object());
		let index = AssetIndexVCurrent::parse(&self.props["assetIndex"]).unwrap();
		let id_dot_json = index.id + ".json";
		let index_path = self.config.gamedir.join("assets").join("indexes").join(&id_dot_json);
		let assets_path = self.config.gamedir.join("assets").join("objects");
		if !index_path.exists() {
			log_action!("Downloading asset index");
			utils::ensure_path(&self.config.gamedir.join("assets"), &Path::new("indexes").join(&id_dot_json));
			utils::download_file(&index.url, &index_path);
		}
		let mut index_file = fs::File::open(index_path).unwrap();
		let index_content = json::read_file(&mut index_file).unwrap();
		for (name, value) in index_content["objects"].iter_obj().unwrap() {
			let hash_name = value["hash"].as_str().unwrap();
			let hash_prefix = &hash_name[..2];
			let path = assets_path.join(hash_prefix).join(hash_name);
			utils::ensure_path(&assets_path, &Path::new(hash_prefix).join(hash_name));
			if !path.exists() {
				log_action!("Downloading asset: {}", name);
				utils::download_file(&format!("https://resources.download.minecraft.net/{hash_prefix}/{hash_name}"), &path);
			}
		}
	}

	fn download_manifest(&self) -> VersionsManifest {
		let manifest_path = self.config.gamedir.join("version_manifest_v2.json");
		let manifest = utils::download_string("https://launchermeta.mojang.com/mc/game/version_manifest_v2.json");
		// Silently ignore if writing to file fails
		let _ = fs::write(manifest_path, &manifest);
		let manifest = json::read_string(&manifest).unwrap();
		VersionsManifest::parse(&manifest).unwrap()
	}

	pub fn show_available_versions(&self) {
		log_info!("Avalable versions:");
		for entry in fs::read_dir(self.config.gamedir.join("versions")).unwrap().filter_map(Result::ok) {
			if !matches!(entry.file_type(), Ok(ft) if ft.is_dir()) {
				continue;
			}
			let mut json = entry.file_name();
			json.push(".json");
			if entry.path().join(json).exists() {
				println!("{:?}", entry.file_name());
			}
		}
	}

	pub fn show_last_version(&self) {
		let manifest = self.download_manifest();
		log_info!("Latest version is: release({}), snapshot({})", manifest.latest_release, manifest.latest_snapshot);
	}

	pub fn cleanup_nonpresent_versions(&self) {
		log_info!("Collecting nonpresent versions");
		for entry in fs::read_dir(self.config.gamedir.join("versions")).unwrap().filter_map(Result::ok) {
			if !matches!(entry.file_type(), Ok(ft) if ft.is_dir()) {
				continue;
			}
			let path = entry.path();
			let mut json = entry.file_name();
			json.push(".json");
			if path.join(json).exists() {
				continue;
			}

			let version_name = entry.file_name().to_string_lossy().into_owned();
			if fs::read_dir(&path).unwrap().next().is_some() {
				log_warn!("Garbáge version contains non-empty directory: {}", version_name);
				continue;
			}

			// TODO: Prevent race conditions?
			log_action!("Deleting empty version: {}", version_name);
			fs::remove_dir(&path).unwrap();
		}
	}

	pub fn download_game(&mut self) {
		let json: PathBuf = self.config.verdir.join(self.config.version.clone() + ".json");
		if !json.exists() {
			let manifest = self.download_manifest();
			let ver = manifest.versions.iter().find(|v| v.id == self.config.version);
			let ver = match ver {
				Some(ver) => ver,
				None => {
					log_err!("Version {} not found. Aborting", self.config.version);
					std::process::exit(1);
				},
			};
			// TODO: Resolve relative to minecraft version dir?
			utils::ensure_path(Path::new(""), &json);
			utils::download_file(&ver.url, &json);
		}
		let mut json = fs::File::open(json).unwrap();
		self.props = json::read_file(&mut json).unwrap();
		let props = &self.props;
		drop(json);
		if !self.config.gamefile.exists() {
			log_action!("Downloading game");
			// TODO: Handle android transfer
			utils::download_file(json_traverse_safe!(props["downloads"]["client"]["url"]).and_then(|f| f.as_str()).unwrap(), &self.config.gamefile);
		}
		self.init_env();
	}

	pub fn compose_args(&self) -> Vec<OsString> {
		use std::iter::once;
		use std::borrow::Borrow;
		once("java")
			.chain(self.jvm_args.iter().map(Borrow::borrow))
			.chain(once(self.props["mainClass"].as_str().unwrap()))
			.chain(self.game_args.iter().map(Borrow::borrow))
			.map(OsString::from)
			.collect()
	}

	pub fn launch_game(&mut self) -> ! {
		use std::process::Command;

		self.construct_args();
		let cmd = self.compose_args();
		let (prog, args) = cmd.split_first().expect("Constructed args must not be empty");
		if !self.config.silent {
			use std::fmt::Write;
			let mut cmd_str = String::new();
			write!(cmd_str, "{:?}", prog).unwrap();
			for arg in args {
				write!(cmd_str, " {:?}", arg).unwrap();
			}
			log_action!("Command line: {}", cmd_str);
		}

		let mut cmd = Command::new(prog);
		cmd.args(args);
		let err = {
			#[cfg(unix)]
			{
				use std::os::unix::process::CommandExt;
				cmd.exec()
			}
			#[cfg(not(unix))]
			{
				match cmd.status() {
					Ok(status) => std::process::exit(status.code().unwrap_or(1)),
					Err(err) => err,
				}
			}
		};

		log_err!("Process failed to start: {}", err);
		std::process::exit(99);
	}
}

/**Structure
	[
		# rule
		{
			"action": "allow" | "disallow"
			#[optional] "os": {
				#[optional] "arch": "x86"
				#[optional] "name": "windows" | "osx" | "macos" | "linux"
			}
			#[optional] "features": {
				#[name]: True | False
			}
		}
	]*/
#[derive(JsonParse)]
pub struct Rule {
	#[json_key]
	action: RuleAction,
	#[json_key(allow_missing)]
	os: Option<RuleOS>,
	#[json_key(allow_missing)]
	features: Option<HashMap<String, bool>>,
}

#[derive(JsonParse)]
enum RuleAction {
	#[json_spelling = "allow"]
	Allow,
	#[json_spelling = "disallow"]
	Disallow,
}

#[derive(JsonParse)]
struct RuleOS {
	#[json_key(allow_missing)]
	arch: Option<RuleArch>,
	#[json_key(allow_missing)]
	name: Option<OSPlatform>,
}

#[allow(non_camel_case_types)]
#[derive(JsonParse, Clone, Copy)]
enum RuleArch {
	x86,
}

impl RuleArch {
	fn check(self, arch: Arch) -> bool {
		match (self, arch) {
			(RuleArch::x86, Arch::x86_32) => true,
			(RuleArch::x86, Arch::x86_64) => true,
			_ => false,
		}
	}
}

#[derive(JsonParse)]
pub struct VersionsManifest {
	#[json_key("latest.release")]
	latest_release: String,
	#[json_key("latest.snapshot")]
	latest_snapshot: String,
	#[json_key]
	versions: Vec<VersionEntry>,
}

#[derive(JsonParse)]
struct VersionEntry {
	#[json_key]
	id: String,
	#[json_key]
	url: String,
}

#[derive(JsonParse)]
pub struct AssetIndexVCurrent {
	#[json_key]
	pub id: String,
	#[json_key]
	pub sha1: String,
	#[json_key]
	pub size: usize,
	#[json_key("totalSize")]
	pub total_ize: usize,
	#[json_key]
	pub url: String,
}

pub struct Library {
	name: String,
	main_artifact: Option<LibraryArtifact>,
	native_artifacts: HashMap<String, LibraryArtifact>,
	rules: Vec<Rule>,
	extract: Option<LibraryExtract>,
}

#[derive(JsonParse)]
pub struct LibraryExtract {
	#[json_key]
	pub exclude: Vec<PathBuf>,
}

impl JsonParse for Library {
	fn parse(tree: &json::JsonValue) -> json::Result<Self> {
		/**Structure
			"downloads": {
				// In older schemas native libraries may be in a seperate entry without the main artifact
				#[optional] "artifact': {
					"path": ...
					"sha1": ...
					"size": ...
					"url": ...
				}
				#[optional] "classifiers": {
					(key): {
						"path": ...
						"sha1": ...
						"size": ...
						"url": ...
					}
				}
			}
			"name": string
			#[optional] "rules": [ Rule ]
			#[optional] "natives": {
				#[optional] "osx" | "linux" | "windows": (key in "downloads.classifiers")
			}
			#[optional] "extract": {
				"exclude": [ path ]
			}
		*/
		#[derive(JsonParse)]
		pub struct RawLibrary {
			#[json_key]
			name: String,
			#[json_key(allow_missing, "downloads.artifact")]
			artifact: Option<LibraryArtifact>,
			#[json_key(allow_missing, "downloads.classifiers")]
			native_artifacts: Option<HashMap<String, LibraryArtifact>>,
			#[json_key(allow_missing)]
			rules: Option<Vec<Rule>>,
			#[json_key(allow_missing)]
			extract: Option<LibraryExtract>,
			#[json_key(allow_missing, "natives")]
			natives_key: Option<HashMap<String, String>>,
		}

		let lib = RawLibrary::parse(tree)?;
		let natives = match (lib.natives_key, lib.native_artifacts) {
			(None, None) => Default::default(),
			(Some(key_map), Some(mut map)) => {
				let mut natives = HashMap::new();
				for (platform, artifact) in key_map.into_iter()
					.map(|(platform, key)| (platform, map.remove(&key)))
				{
					if let Some(artifact) = artifact {
						natives.insert(platform, artifact);
					} else {
						log_warn!("Native library {} has no corresponding platform key for {}",
							lib.name, platform);
					}
					
				}
				/*if !map.is_empty() {
					log_warn!("Native library index contains unused keys");
				}*/
				natives
			}
			_ => return Err(json::ParseError),
		};

		Ok(Library {
			name: lib.name,
			main_artifact: lib.artifact,
			native_artifacts: natives,
			rules: lib.rules.unwrap_or_default(),
			extract: lib.extract,
		})
	}
}

#[derive(JsonParse)]
pub struct LibraryArtifact {
	#[json_key]
	pub path: PathBuf,
	#[json_key]
	pub sha1: String,
	#[json_key]
	pub size: usize,
	#[json_key]
	pub url: String,
}
