pub mod launcher;
pub mod utils;
pub(crate) mod argparse;
mod json;

use std::path::PathBuf;
use launcher::Platform;
use argparse::{parser, Parser};

fn main() {
	let mut arg_parser = parser!{
		description: "Run Minecraft game",
		arguments: [
			version { help: "version of the game to be targeted" },
			//dump: bool => "--dump" { help: "only dump launch properties, don't run the game" },
			player => "--player" { help: "player's name", default: "Player".to_owned() },
			silent: bool => "--silent" { help: "show only output, no messages" },
			classpath: Vec<String> => "--classpath" { help: "additional classpath to join with default one" },
			jvm_args: Vec<String> => "--java-arg" { help: "additional arguments to java runtime" },
			game_dir: PathBuf => "--game-dir" { help: "game root directory", default: utils::default_minecraft_dir() },
			platform: Option<Platform> => {
				"--desktop" => Platform::Desktop; { help: "force behaviour as on desktop environment (default is autodetect)" },
				"--android" => Platform::Android; { help: "force behaviour as on Android environment (default is autodetect)" },
			}
		],
	};
	let args: Vec<String> = std::env::args().skip(1).collect();
	let args: Vec<&str> = args.iter().map(|x| x.as_str()).collect();
	let args = arg_parser.parse(args.as_slice());
	let platform = args.platform.unwrap_or_else(|| if utils::detect_android() { launcher::Platform::Android } else { launcher::Platform::Desktop });
	if platform == launcher::Platform::Android {
		log_info!("Using Android version");
		log_err!("Android is not yet implemented");
		std::process::exit(1);
	}
	let gamedir = args.game_dir;
	let verdir = gamedir.join("versions").join(&args.version);
	enum Action {
		ShowLatest,
		ListVersions,
		CleanupVersions,
		Launch,
	}
	let action = match args.version.as_str() {
		"latest" => Action::ShowLatest,
		"list" => Action::ListVersions,
		"clean-versions" => Action::CleanupVersions,
		_ => Action::Launch,
	};
	let mut launcher = launcher::McLauncher::create(
		launcher::LauncherConfig {
			launcher: "tbrew",
			arch: launcher::Arch::current(),
			os: launcher::OSPlatform::current(),
			platform,
			player: args.player,
			libdir: gamedir.join("libraries"),
			gamedir,
			gamefile: verdir.join(args.version.clone() + ".jar"),
			version: args.version,
			verdir,
			auth: None,
			jvm_args: args.jvm_args,
			game_args: Vec::new(),
			silent: args.silent,
			jvm_classpath: args.classpath,
		}
	);
	launcher.ensure_gamedir();
	match action {
		Action::ShowLatest => {
			launcher.show_last_version();
		}
		Action::ListVersions => {
			launcher.show_available_versions();
		}
		Action::CleanupVersions => {
			launcher.cleanup_nonpresent_versions();
		}
		Action::Launch => {
			log_info!("Updating the game...");
			launcher.download_game();
			log_info!("Updating assets...");
			launcher.download_assets();
			log_info!("Updating libraries...");
			launcher.download_libraries();
			log_info!("Running game...");
			launcher.launch_game();
		}
	}
}
