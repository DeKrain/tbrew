use std::path::{Path, PathBuf};
use std::fs;
use std::io;
use reqwest as rq;

macro_rules! logger {
	($macro: ident $prefix: literal) => { logger!($macro $prefix $); };
	($macro: ident $prefix: literal $dol: tt) => {
		#[macro_export]
		macro_rules! $macro {
			($dol format: literal $dol (, $dol($dol arg:expr),+)?) => {
				{
					println!(concat!($prefix, $dol format) $dol(, $dol($dol arg),+)?)
				}
			};
		}
	};
}

logger!(log_action "\x1b[92m[!]\x1b[m ");
logger!(log_info "\x1b[96m[!]\x1b[m ");
logger!(log_err "\x1b[91m[!]\x1b[m ");
logger!(log_warn "\x1b[93m[!]\x1b[m ");

pub mod loggers {
	pub use log_action;
	pub use log_info;
	pub use log_err;
	pub use log_warn;
}

#[derive(Debug)]
pub enum FileDownloadError {
	Io(io::Error),
	Reqwest(rq::Error),
}

impl From<io::Error> for FileDownloadError {
	fn from(error: io::Error) -> Self {
		FileDownloadError::Io(error)
	}
}

impl From<rq::Error> for FileDownloadError {
	fn from(error: rq::Error) -> Self {
		FileDownloadError::Reqwest(error)
	}
}

impl std::fmt::Display for FileDownloadError {
	fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
		f.write_str("Download failed: ")?;
		match self {
			FileDownloadError::Io(error) => error.fmt(f),
			FileDownloadError::Reqwest(error) => error.fmt(f),
		}
	}
}

impl std::error::Error for FileDownloadError {
	fn source(&self) -> Option<&(dyn std::error::Error + 'static)> {
		match self {
			FileDownloadError::Io(error) => Some(error),
			FileDownloadError::Reqwest(error) => Some(error),
		}
	}
}

pub fn try_download_string(url: &str) -> Result<String, FileDownloadError> {
	// TODO: Integrate async runtime and process requests in parallel (improvement over pybrew)
	Ok(rq::blocking::get(url)?.text()?)
}

pub fn try_download_file(url: &str, path: &Path) -> Result<(), FileDownloadError> {
	// TODO: Integrate async runtime and process requests in parallel (improvement over pybrew)

	// Use create_new to avoid possibly overriding important files
	let mut file = fs::File::options()
		.write(true)
		.create_new(true)
		.open(path)?;

	let mut response = rq::blocking::get(url)?;
	io::copy(&mut response, &mut file)?;
	Ok(())
}

pub fn download_file(url: &str, path: &Path) {
	match try_download_file(url, path) {
		Ok(res) => res,
		Err(error) => {
			log_err!("{}", error);
			std::process::exit(1);
		},
	}
}

pub fn download_string(url: &str) -> String {
	match try_download_string(url) {
		Ok(res) => res,
		Err(error) => {
			log_err!("{}", error);
			std::process::exit(1);
		},
	}
}

#[allow(unused_variables)]
pub fn extract_zip(path: &Path, out: &Path, exclude: &[PathBuf]) {
	// TODO
}

pub fn ensure_path(base: &Path, relpath: &Path) {
	fs::create_dir_all(base.join(relpath.parent().unwrap_or(Path::new("")))).unwrap()
}

// From Python version
// There should be a clearer way of doing this
pub fn detect_android() -> bool {
	Path::new("/system/bin/dalvikvm").exists()
}

pub fn default_minecraft_dir() -> PathBuf {
	// Ref: https://minecraft.fandom.com/wiki/.minecraft#Locating_.minecraft

	#[cfg(unix)]
	{
		let home = std::env::var_os("HOME").expect("No home directory set");
		#[cfg(target_os = "macos")]
		{
			let mut path = PathBuf::from(home);
			path.extend(["Library", "Application Support", "minecraft"]);
			path
		}
		#[cfg(not(target_os = "macos"))]
		{
			PathBuf::from(home).join(".minecraft")
		}
	}
	#[cfg(windows)]
	{
		let appdata = std::env::var_os("APPDATA").expect("Appdata directory not set");
		PathBuf::from(appdata).join(".minecraft")
	}
	#[cfg(not(any(unix, windows)))]
	{
		panic!("Target not supported")
	}
}
