// Custom minimal argument parse library

/*#[macro_export]
macro_rules! parser {
	{
		description: $description: literal,
		arguments: [
			$(
				$param: ident $(
					: $type: ty
				)? $(
					=> $($lit: literal),*
				)? $(
					{
						$(help: $help: literal $(,)?)?
						$(default: $default: expr $(,)?)?
					}
				)?
				,
			)*
		],
	} => {};
}*/

use std::{marker::PhantomData, path::PathBuf};

#[macro_export]
macro_rules! argument_opt {
	(help: $help: literal) => {};
	(default: $default: expr) => {};
}

#[macro_export(local_inner_macros)]
macro_rules! argument_match {
	($str: literal) => {
		match_argument($str);
	};
	([ $($str: literal),+ $(,)? ]) => {
		match_argument($($str),+);
	};
	({
		$(
			$str: literal => $value: expr
			$(; {
				$($opt: ident: $opt_body: expr),* $(,)?
			})?
		),+ $(,)?
	}) => {
		$(
			//match_case($str, $value);
			$($(
				argument_opt!($opt: $opt_body);
			)*)?
		)+
	};
}

/*macro_rules! argument_type {
	{
		fixed: $fixed: ty,
		$($_: tt)*
	} => { $fixed };
	{
		default: $default: expr,
		$($_: tt)*
	} => { () };
	{
		$name: ident: $v: expr,
		$($more: tt)*
	} => { argument_type!{$($more)*} };
}*/
macro_rules! argument_type {
	($type: ty) => {
		$type
	};
	() => {
		String
	};
} pub(crate) use argument_type;

pub type ArgumentState<T> = <T as StateType>::State;

pub trait StateType {
	type State: Default + State<Target = Self> + Activation;
}

pub trait State {
	type Target;

	fn unwrap(self, name: &str) -> Self::Target;
	fn unwrap_or_else(self, default: impl FnOnce() -> Self::Target) -> Self::Target;
}

pub trait Activation {
	type Activate;
	fn activate_value<'a>(&mut self, value: Self::Activate);
}

pub trait ArgumentActivation {
	fn activate<'a>(&mut self, it: impl Iterator<Item = &'a str>);
}

// Renamed from DynArgumentActivation to highlight use with single parameter (positional arg)
pub trait PositionalArgumentActivation {
	fn activate<'a>(&mut self, arg: &'a str);
}

impl<T: ArgumentActivation> PositionalArgumentActivation for T {
	fn activate<'a>(&mut self, arg: &'a str) {
		ArgumentActivation::activate(self, core::iter::once(arg));
	}
}

// Rust Traits rules are karens
fn _default_activate<'a, T: Activation>(this: &mut T, it: &mut impl Iterator<Item = &'a str>)
where T::Activate: From<&'a str> {
	this.activate_value(it.next().expect("Missing argument for parameter ...").into());
}

impl<T> StateType for Option<T> {
	type State = Option<T>;
}

impl<T> StateType for Vec<T> {
	type State = Vec<T>;
}

/*impl<T: std::marker::DiscriminantKind> StateType for T {
	type State = RequiredArgumentState<T>;
}*/

/*impl<T> StateType for Option<Vec<T>> {
	type State = VecState<T>;
}*/

impl StateType for String {
	type State = RequiredArgumentState<String>;
}

impl StateType for PathBuf {
	type State = RequiredArgumentState<PathBuf>;
}

impl StateType for bool {
	type State = bool;
}

impl<T> State for Option<T> {
	type Target = Option<T>;

	#[inline(always)]
	fn unwrap(self, name: &str) -> Option<T> {
		self
	}

	#[inline(always)]
	fn unwrap_or_else(self, default: impl FnOnce() -> Self::Target) -> Self::Target {
		self.or_else(default)
	}
}

impl<T> Activation for Option<T> {
	type Activate = T;

	fn activate_value(&mut self, value: T) {
		match self {
			Some(_) => panic!("Value already set"),
			None => *self = Some(value),
		}
	}
}

impl<T> ArgumentActivation for Option<T>
where T: for<'a> From<&'a str> {
	#[inline(always)]
	fn activate<'a>(&mut self, mut it: impl Iterator<Item = &'a str>) {
		_default_activate(self, &mut it)
	}
}

pub enum RequiredArgumentState<T> {
	Unset,
	Set(T),
}

impl<T> Default for RequiredArgumentState<T> {
	#[inline(always)]
	fn default() -> Self {
		Self::Unset
	}
}

impl<T> State for RequiredArgumentState<T> {
	type Target = T;

	fn unwrap(self, name: &str) -> T {
		use RequiredArgumentState::*;
		match self {
			Set(v) => v,
			Unset => panic!("Missing parameter: {name}"),
		}
	}

	#[inline(always)]
	fn unwrap_or_else(self, default: impl FnOnce() -> Self::Target) -> Self::Target {
		use RequiredArgumentState::*;
		match self {
			Set(v) => v,
			Unset => default(),
		}
	}
}

impl<T> Activation for RequiredArgumentState<T> {
	type Activate = T;

	fn activate_value<'a>(&mut self, value: T) {
		use RequiredArgumentState::*;
		match self {
			Set(_) => panic!("Value already set"),
			Unset => *self = Set(value),
		}
	}
}

impl<T> ArgumentActivation for RequiredArgumentState<T>
where T: for<'a> From<&'a str> {
	#[inline(always)]
	fn activate<'a>(&mut self, mut it: impl Iterator<Item = &'a str>) {
		_default_activate(self, &mut it)
	}
}

impl<T> State for Vec<T> {
	type Target = Vec<T>;

	#[inline(always)]
	fn unwrap(self, name: &str) -> Vec<T> {
		self
	}

	#[inline(always)]
	fn unwrap_or_else(self, default: impl FnOnce() -> Self::Target) -> Self::Target {
		// Allow for some initial sequence if the sequence is empty
		if self.is_empty() {
			default()
		} else {
			self
		}
	}
}

impl<T> Activation for Vec<T> {
	type Activate = T;

	#[inline(always)]
	fn activate_value(&mut self, value: T) {
		self.push(value);
	}
}

impl<T> ArgumentActivation for Vec<T>
where T: for<'a> From<&'a str> {
	#[inline(always)]
	fn activate<'a>(&mut self, mut it: impl Iterator<Item = &'a str>) {
		_default_activate(self, &mut it)
	}
}

impl State for bool {
	type Target = bool;

	#[inline(always)]
	fn unwrap(self, name: &str) -> Self::Target {
		self
	}

	#[inline(always)]
	fn unwrap_or_else(self, default: impl FnOnce() -> Self::Target) -> Self::Target {
		self || default()
	}
}

impl Activation for bool {
	type Activate = ();

	#[inline(always)]
	fn activate_value(&mut self, value: ()) {
		// This ignores duplicates
		*self = true;
	}
}

impl ArgumentActivation for bool {
	#[inline(always)]
	fn activate<'a>(&mut self, it: impl Iterator<Item = &'a str>) {
		self.activate_value(());
	}
}

macro_rules! argument_state {
	(Option<$T: ty>) => {
		None::<$T>
	};
	(Vec<$T: ty>) => {
		Vec::<$T>::new()
	};
	(bool) => {
		false
	};
	($T: ty) => {
		$crate::argparse::ArgumentOption::<$T>::Unset
	}
}

macro_rules! argument_unpack_state {
	($i: ident: Option<$T: ty>) => {
		$i
	};
	($i: ident: Vec<$T: ty>) => {
		$i
	};
	($i: ident: $T: ty) => {
		$i.unwrap(stringify!($i))
	}
}

macro_rules! argument_help_msg {
	{ help: $help:literal, $($extra:tt)* }
	=> { $help };
	{ help: $($extra:tt)* }
	=> { ::std::compile_error!("Help must be a literal") };
	{ $opt:ident: $value:expr, $($extra:tt)+ }
	=> { argument_help_msg!($($extra)*) };
	{ $($any:tt)* }
	=> { "" }
} pub(crate) use argument_help_msg;

macro_rules! argument_help {
	{
		$name: ident
		{ $($opt:ident: $value:expr,)* }
	} => {
		::std::println!("{}", ::std::concat!(
			"<", ::std::stringify!($name), "> (positional)\n\t",
			crate::argparse::argument_help_msg!($($opt: $value,)*)
		));
	};
	{
		$name: ident => {
			$(
				$match: literal => $_val: expr $(; { $($opt:ident: $val:expr),* $(,)? })?
			),+ $(,)?
		}
		{ $($_opts:ident: $_val_:expr,)* }
	} => {
		$(crate::argparse::argument_help! {
			x => $match { $($($opt: $val,)*)? }
		};)+
	};
	{
		$name: ident => $match: tt
		{ $($opt:ident: $value:expr,)* }
	} => {
		::std::println!("{}", ::std::concat!(
			$match, "\n\t",
			crate::argparse::argument_help_msg!($($opt: $value,)*)
		));
	};
} pub(crate) use argument_help;

macro_rules! argument_parse_match {
	// Terminator
	{
		@match $scrutinee:expr =>
		@arms [$($arms:tt)*]
		@tail [$($tail:tt)*]
		@params []
	} => {
		match $scrutinee {
			$($arms)*
			$($tail)*
		}
	};

	// Missing @match
	{
		@match $scrutinee:expr =>
		@arms [$($arms:tt)*]
		@tail [$($tail:tt)*]
		@params [
			$param:ident
			//$(@type $type:ty ;)?
			$(@action $action:expr ;)?
			$($next:ident $($more:tt)*)?
		]
	} => {
		crate::argparse::argument_parse_match! {
			@match $scrutinee =>
			@arms [$($arms)*]
			@tail [$($tail)*]
			@params [
				$param
				@match (@concat "--", ::std::stringify!($param))
				//$(@type $type ;)?
				$(@action $action ;)?
				$($next $($more)*)?
			]
		}
	};

	// Missing @type
	/*{
		@match $scrutinee:expr =>
		@arms [$($arms:tt)*]
		@tail [$($tail:tt)*]
		@params [
			$param:ident
			@match $match:tt
			$($next:ident $($more:tt)*)?
		]
	} => {
		crate::argparse::argument_parse_match! {
			@match $scrutinee =>
			@arms [$($arms)*]
			@tail [$($tail)*]
			@params [
				$param
				@match $match
				//@type String ;
				$($next $($more)*)?
			]
		}
	};*/

	// Missing @action
	/*{
		@match $scrutinee:expr =>
		@arms [$($arms:tt)*]
		@tail [$($tail:tt)*]
		@params [
			$param:ident
			@match $match:tt
			//@type $type:ty ;
			$(@action: $action:expr ;)?
			$($next:ident $($more:tt)*)?
		]
	} => {
		crate::argparse::argument_parse_match! {
			@match $scrutinee =>
			@arms [$($arms)*]
			@tail [$($tail)*]
			@params [
				$param
				@match $match
				//@type $type ;
				@action $param .activate() ;
				$($next $($more)*)?
			]
		}
	};*/

	// Template
	/*{
		@match $scrutinee:expr =>
		@arms [$($arms:tt)*]
		@tail [$($tail:tt)*]
		@params [
			$param:ident
			$(@match $match:tt)?
			$(@type $type:ty ;)?
			$($next:ident $($more:tt)*)?
		]
	} => {
		crate::argparse::argument_parse_match! {
			@match $scrutinee =>
			@arms [$($arms)*]
			@tail [$($tail)*]
			@params [
				$($next $($more)*)?
			]
		}
	};*/

	{
		@match $scrutinee:expr =>
		@arms [$($arms:tt)*]
		@tail [$($tail:tt)*]
		@params [
			$param:ident
			@match $match:literal
			//@type $type:ty ;
			@action $action:expr ;
			$($next:ident $($more:tt)*)?
		]
	} => {
		crate::argparse::argument_parse_match! {
			@match $scrutinee =>
			@arms [$($arms)*]
			@tail [$($tail)*]
			@params [
				@pattern $match =>
				@action $action ;
				$($next $($more)*)?
			]
		}
	};

	{
		@match $scrutinee:expr =>
		@arms [$($arms:tt)*]
		@tail [$($tail:tt)*]
		@params [
			$param:ident
			@match (@concat $($match:expr),* $(,)?)
			//@type $type:ty ;
			@action $action:expr ;
			$($next:ident $($more:tt)*)?
		]
	} => {
		crate::argparse::argument_parse_match! {
			@match $scrutinee =>
			@arms [$($arms)*]
			@tail [$($tail)*]
			@params [
				@pattern ::std::concat!($($match),*) =>
				@action $action ;
				$($next $($more)*)?
			]
		}
	};

	{
		@match $scrutinee:expr =>
		@arms [$($arms:tt)*]
		@tail [$($tail:tt)*]
		@params [
			$param:ident
			@match [ $($match:literal),+ $(,)? ]
			//@type $type:ty ;
			@action $action:expr ;
			$($next:ident $($more:tt)*)?
		]
	} => {
		crate::argparse::argument_parse_match! {
			@match $scrutinee =>
			@arms [$($arms)*]
			@tail [$($tail)*]
			@params [
				@pattern $($match)|+ =>
				@action $action ;
				$($next $($more)*)?
			]
		}
	};

	{
		@match $scrutinee:expr =>
		@arms [$($arms:tt)*]
		@tail [$($tail:tt)*]
		@params [
			$param:ident
			@match { $(
				$sub_match:tt => $sub_expr: expr ; $({ $($opts:tt)* })?
			),+ $(,)? }
			//@type $type:ty ;
			@action $action:expr ;
			$($next:ident $($more:tt)*)?
		]
	} => {
		crate::argparse::argument_parse_match! {
			@match $scrutinee =>
			@arms [$($arms)*]
			@tail [$($tail)*]
			@params [
				$(
					$param
					@match $sub_match
					//@type $type ;
					@action <_ as crate::argparse::Activation>::activate_value(&mut *$param.borrow_mut(), $sub_expr) ;
				)+
				$($next $($more)*)?
			]
		}
	};

	{
		@match $scrutinee:expr =>
		@arms [$($arms:tt)*]
		@tail [$($tail:tt)*]
		@params [
			@pattern $pattern: pat =>
			@action $action: expr ;
			$($next:ident $($more:tt)*)?
		]
	} => {
		crate::argparse::argument_parse_match! {
			@match $scrutinee =>
			@arms [
				$($arms)*
				$pattern => $action,
			]
			@tail [$($tail)*]
			@params [
				$($next $($more)*)?
			]
		}
	};
} pub(crate) use argument_parse_match;

macro_rules! argument_select_default {
	({$($opts:tt)*}
		@some $isome:ident => $some:tt
		@none => $none:tt
	) => {
		crate::argparse::argument_select_default!(
			($) {$($opts)*}
			@some $isome => $some
			@none => $none
		)
	};
	(($dol:tt) {
		default: $default: expr $(,
			$($opt: ident: $opt_body: expr),* $(,)?
		)?
	}
	@some $isome:ident => $some:tt
	@none => $none:tt
	) => {{
		macro_rules! x__ {
			($dol $isome:tt) => { $some }
		}
		x__!($default)
	}};
	(($dol:tt) {
		$_o:ident: $_b:expr $(,
			$($opt: ident: $opt_body: expr),* $(,)?
		)?
	}
	@some $isome:ident => $some:tt
	@none => $none:tt
	) => {
		crate::argparse::argument_select_default!(
			($dol) {$($($opt: $opt_body),*)?}
			@some $isome => $some
			@none => $none
		)
	};
	(($dol:tt) {}
	@some $isome:ident => $some:tt
	@none => $none:tt
	) => {
		$none
	};
} pub(crate) use argument_select_default;

macro_rules! positional_args {
	($($front:ident $($tail:tt)*)?) => {
		crate::argparse::positional_args! {
			@ [] $($front $($tail)*)?
		}
	};
	// null case
	(@ [ $($payload:tt)* ]) => {[ $($payload)* ]};
	// non-positional
	(@ [ $($payload:tt)* ] $param:ident => $match:tt $($more:tt)*) => {
		crate::argparse::positional_args! { @ [ $($payload)* ] $($more)* }
	};
	// positional
	(@ [ $($payload:tt)* ] $param:ident $($more:tt)*) => {
		crate::argparse::positional_args! {
			@ [
				$($payload)*
				&$param,
			]
			$($more)*
		}
	};
} pub(crate) use positional_args;

macro_rules! argument {
	[
		$(
			$param: ident
			$(: $type: ty)?
			$(=> $match: tt)?
			$({
				$($opt: ident: $opt_body: expr),* $(,)?
			})?
		),* $(,)?
	] => {
		{
			use std::marker::PhantomData;
			struct ParseResult {
				$(
					$param: crate::argparse::argument_type!{
						$($type)?
					},
				)*
			}
			impl ParseResult {
				fn show_help_and_exit(status: i32, parser: &Parser<Self>) -> ! {
					::std::println!("{}\nAvailable options:", parser.description());
					$(crate::argparse::argument_help!(
						$param $(=> $match)?
						{$($($opt: $opt_body,)*)?}
					);)*
					std::process::exit(status);
				}
				fn unknown_option(opt: &str, parser: &Parser<Self>) -> ! {
					::std::eprintln!("Unknown parameter: {opt}");
					Self::show_help_and_exit(1, parser);
				}
			}
			impl $crate::argparse::ArgumentParser for ParseResult {
				fn parse(parser: &mut Parser<Self>, args: &[&str]) -> Self {
					use ::core::cell::RefCell;
					$(
						let mut $param = RefCell::new(crate::argparse::ArgumentState::<crate::argparse::argument_type!($($type)?)>::default());
					)*
					let mut arg_it = args.iter().copied();
					#[allow(non_camel_case_types)]
					trait __force_array<T> {
						#[inline(always)]
						fn __force_array<const N: usize>(arr: [T; N]) -> [T; N] {
							arr
						}
					}
					impl<T> __force_array<T> for [T] {}
					let mut positionals = <[&RefCell<dyn crate::argparse::PositionalArgumentActivation>]>::__force_array(
						crate::argparse::positional_args! {
							$($param $(=> $match)?)*
						}
					).into_iter();
					while let Some(arg) = arg_it.next() {
						crate::argparse::argument_parse_match! {
							@match arg =>
							@arms [
								"--help" => Self::show_help_and_exit(0, parser),
							]
							@tail [
								s if s.starts_with('-') => Self::unknown_option(s, parser),
								//s => Self::positional_arg(&mut s),
								s => {
									let param = positionals.next().expect("No positional parameter to match this argument");
									param.borrow_mut().activate(s);
								}
							]
							@params [$(
								$param $(@match $match)? /*$(@type $type ;*)?*/
								@action crate::argparse::ArgumentActivation::activate(&mut *$param.borrow_mut(), &mut arg_it) ;
							)*]
						}
					}
					{ positionals };
					Self {
						$(
							//$param: crate::argparse::argument_unpack_state!($param: argument_type!($($type)?)),
							$param: crate::argparse::argument_select_default!(
								{ $($($opt: $opt_body),*)? }
								@some default => (crate::argparse::State::unwrap_or_else($param.into_inner(), || $default))
								@none => (crate::argparse::State::unwrap($param.into_inner(), stringify!($param)))
							),
						)*
					}
				}
			}
			/*$(
				let $param $(: $type)*;
				$(argument_match!($match);)?
				$($(
					argument_opt!($opt: $opt_body);
				)*)?
			)*/
			PhantomData::<ParseResult>
		}
	}
} pub(crate) use argument;

macro_rules! parser_item {
	(description: $descr: literal) => {
		String::from($descr)
	};
	(arguments: $body: tt) => {
		crate::argparse::argument! $body
	};
} pub(crate) use parser_item;

macro_rules! parser {
	{
		$($item: ident: $body: tt),+
		$(,)?
	} => {
		{
			//$crate::create_parser_from_description!()
			crate::argparse::Parser::new($crate::argparse::ParserConfig {
				$(
					$item: crate::argparse::parser_item!($item: $body),
				)*
			})
		}
	}
} pub(crate) use parser;

pub trait ArgumentParser : Sized {
	fn parse(parser: &mut Parser<Self>, args: &[&str]) -> Self;
}

pub struct ParserConfig<T> {
	pub description: String,
	pub arguments: PhantomData<T>,
}

pub struct Parser<T: ArgumentParser> {
	_pd: PhantomData<*const T>,
	descr: String,
}

impl<T: ArgumentParser> Parser<T> {
	pub fn new(config: ParserConfig<T>) -> Self {
		Parser { descr: config.description, _pd: PhantomData }
	}
	pub fn description(&self) -> &str {
		self.descr.as_str()
	}
	pub fn parse(&mut self, args: &[&str]) -> T { T::parse(self, args) }
}
