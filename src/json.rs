extern crate json;

use std::io::{self, Read};
pub use json::JsonValue;
pub use json::Result as ParseResult;
use std::collections::HashMap;

#[derive(Debug)]
pub enum ReadFileError {
	Io(io::Error),
	Parse(json::Error),
}

impl From<io::Error> for ReadFileError {
	fn from(error: io::Error) -> Self {
		ReadFileError::Io(error)
	}
}

impl From<json::Error> for ReadFileError {
	fn from(error: json::Error) -> Self {
		ReadFileError::Parse(error)
	}
}

impl std::fmt::Display for ReadFileError {
	fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
		match self {
			ReadFileError::Io(error) => error.fmt(f),
			ReadFileError::Parse(error) => error.fmt(f),
		}
	}
}

pub fn read_file(file: &mut std::fs::File) -> std::result::Result<JsonValue, ReadFileError> {
	let mut contents = String::new();
	file.read_to_string(&mut contents)?;
	Ok(read_string(&contents)?)
}

pub fn read_string(str: &str) -> ParseResult<JsonValue> {
	json::parse(str)
}

pub type Result<T> = std::result::Result<T, ParseError>;

#[derive(Debug)]
pub struct ParseError;

impl std::fmt::Display for ParseError {
	fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
		f.write_str("Parsing JSON data failed")
	}
}

pub trait JsonParse where Self: Sized {
	fn parse(tree: &JsonValue) -> Result<Self>;
}

pub trait JsonExtensions {
	fn safe_index_obj(&self, key: &str) -> Option<&Self>;
	fn safe_index_arr(&self, index: usize) -> Option<&Self>;
	fn iter_obj(&self) -> Option<json::iterators::Entries>;
	fn iter_arr(&self) -> Option<json::iterators::Members>;
	fn collect_arr<T, F> (&self, transform: F) -> Result<Vec<T>>
		where F: FnMut(&Self) -> Result<T>;
	fn collect_obj<T, F>(&self, transform: F) -> Result<HashMap<String, T>>
		where F: FnMut(&Self) -> Result<T>;
	fn null_or<T, F>(&self, transform: F) -> Result<Option<T>>
		where F: FnOnce(&Self) -> Result<T>;
}

impl JsonExtensions for json::JsonValue {
	#[inline]
	fn safe_index_obj(&self, key: &str) -> Option<&Self> {
		match self {
			json::JsonValue::Object(ref val) => val.get(&key),
			_ => None,
		}
	}

	#[inline]
	fn safe_index_arr(&self, key: usize) -> Option<&Self> {
		match self {
			json::JsonValue::Array(ref val) => val.get(key),
			_ => None,
		}
	}

	#[inline]
	fn iter_obj(&self) -> Option<json::iterators::Entries> {
		match self {
			json::JsonValue::Object(ref val) => Some(val.iter()),
			_ => None,
		}
	}

	#[inline]
	fn iter_arr(&self) -> Option<json::iterators::Members> {
		match self {
			json::JsonValue::Array(ref val) => Some(val.iter()),
			_ => None,
		}
	}

	#[inline]
	fn collect_arr<T, F>(&self, mut transform: F) -> Result<Vec<T>>
	where F: FnMut(&Self) -> Result<T> {
		let iter = self.iter_arr().ok_or(ParseError)?;
		let mut vec = Vec::new();
		for item in iter {
			vec.push(transform(item)?)
		}
		Ok(vec)
	}

	#[inline]
	fn collect_obj<T, F>(&self, mut transform: F) -> Result<HashMap<String, T>>
	where F: FnMut(&Self) -> Result<T> {
		let iter = self.iter_obj().ok_or(ParseError)?;
		let mut map = HashMap::new();
		for (key, val) in iter {
			map.insert(key.into(), transform(val)?);
		}
		Ok(map)
	}

	#[inline]
	fn null_or<T, F>(&self, transform: F) -> Result<Option<T>>
	where F: FnOnce(&Self) -> Result<T> {
		if self.is_null() {
			Ok(None)
		} else {
			match transform(self) {
				Ok(v) => Ok(Some(v)),
				Err(e) => Err(e),
			}
		}
	}
}

pub trait JsonOptionExt<'a> {
	fn missing_or<T, F>(self, transform: F) -> Result<Option<T>>
		where F: FnOnce(&'a JsonValue) -> Result<T>;
}

impl<'a> JsonOptionExt<'a> for Option<&'a JsonValue> {
	#[inline]
	fn missing_or<T, F>(self, transform: F) -> Result<Option<T>>
	where F: FnOnce(&'a JsonValue) -> Result<T> {
		match self {
			None => Ok(None),
			Some(x) => {
				match transform(x) {
					Ok(v) => Ok(Some(v)),
					Err(e) => Err(e),
				}
			}
		}
	}
}

#[cfg(test)]
#[allow(dead_code)]
mod test {
	use crate::json::{self, Result, JsonParse, ParseError, JsonExtensions, JsonOptionExt};

	#[test]
	pub fn example() {
		struct MyData {
			foo: String,
			bar: Vec<MyItem>,
		}
		struct MyItem {
			name: String,
			value: json::JsonValue,
		}
		impl JsonParse for MyData {
			fn parse(tree: &json::JsonValue) -> Result<Self> {
				Ok(Self {
					foo: tree.safe_index_obj("foo").ok_or(ParseError)?.as_str().ok_or(ParseError)?.into(),
					bar: tree.safe_index_obj("egg").ok_or(ParseError)?.safe_index_obj("xyz").ok_or(ParseError)?.collect_arr(
						|item| MyItem::parse(item)
					)?,
				})
			}
		}
		impl JsonParse for MyItem {
			fn parse(tree: &json::JsonValue) -> Result<Self> {
				Ok(Self {
					name: tree.safe_index_obj("name").ok_or(ParseError)?.as_str().ok_or(ParseError)?.into(),
					value: tree.safe_index_obj("value").ok_or(ParseError)?.clone(),
				})
			}
		}

		struct MyMap {
			description: Option<String>,
			data: std::collections::HashMap<String, MyItem>,
		}
		impl JsonParse for MyMap {
			fn parse(tree: &json::JsonValue) -> Result<Self> {
				Ok(Self {
					description: tree.safe_index_obj("description").missing_or(|cont| Ok(cont.as_str().ok_or(ParseError)?.into()))?,
					data: tree.safe_index_obj("data").ok_or(ParseError)?.collect_obj(|item| MyItem::parse(item))?,
				})
			}
		}
	}
}

#[macro_export]
macro_rules! json_traverse_safe {
	($base: tt [$first: expr] $([$key: expr])*) => {
		$base.safe_index_obj($first)$(.and_then(|cont| cont.safe_index_obj($key)))*
	};
}
