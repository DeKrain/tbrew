## T-brew, lightweight Minecraft Launcher

This is a rewrite of my previous launcher project written in Python called pybrew (not released).
Pybrew has a limited support for launching on the Android Operating System, but it's not finished.
Tbrew will get it too in some time.

## Features
Tbrew can launch roughly any version available in Minecraft Launcher, with varying support
for old versions. It can fetch all resources required to run the client, that is the client itself,
libraries, and assets.

Support for downloading **servers** might be added in the future.

Support for launcher profiles (as generated in the main launcher and other tools) might be added in the future.
In the meantime, to run a modded version (such as forge), pass the modded version id as the `--version` argument.

You can specify configuration options on the command-line, such as:
option | description | notes
--- | --- | ---
`--version` | Select the game version, or `list` to show installed versions, or `latest` to fetch latest (release & snaphot) versionumbers | Required |
`--player` | Sets the ingame player name | Default is `Player`
`--game-dir` | Select minecraft installation directory other than the default |

To see other options, run with the argument `--help`.
